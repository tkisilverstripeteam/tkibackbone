/* TkiLib.BModel v.0.0.2
 * Copyright (c) 2013 Todd Hossack, Tiraki 
 * http://www.tiraki.com/
 * Dependencies: jQuery, underscore.js, backbone.js, TkiLib.Utils
 * Last updated: 2013-06-12
 */
if(typeof(TkiLib) === 'undefined') TkiLib = {};
TkiLib.BModel = Backbone.Model.extend({
	idAttribute: "ID",
	/* -------- Instance methods -------- */
	initialize: function() {
			// Setup instance properties
		var options = arguments[1] || {};
		// Merge in allowed properties from options
		_.extend(this, _.pick(options,this.allowedOptions()));
		if(this.debug) this.debug.addToLog("TkiLib.BModel.initialize");
		this._required = [];
		if(options.required) this.setRequired(options.required);
		this._progressIndex = 0;
	},
	allowedOptions: function() {
		return ['modelName','debug'];
	},
	setRequired: function(attrKeys) {
		this._required = _.isArray(attrKeys) ? attrKeys : [attrKeys];
	},
	/* addRequired function
	 * Adds field names to required array. If index is specified, names
	 * are inserted at that point to allow ordered validation.
	 */
	addRequired: function(names,index) {
		if(!names) return;
		names = _.isArray(names) ? names : [names];
		var toAdd = _.difference(names,this._required);
		if(_.isEmpty(toAdd)) return;
		if(_.isNumber(index)) {
			this._required.splice.apply(this._required,[index,0].concat(names));
		} else {
			this._required = this._required.concat(toAdd);
		}
	},
	removeRequired: function(names) {
		var names = _.isArray(names) ? names : [names];
		this._required = _.difference(this._required,names);
	},
	getFromSchema: function(name,key) {
		var v = "";
		if(_.isObject(this.schema) && _.has(this.schema,name)
			&& _.isObject(this.schema[name]) && _.has(this.schema[name],key)) {
			v = this.schema[name][key];
		}
		return v;
	},
	getFormattedValue: function(name) {
		var type = this.getFromSchema(name,"type");
		var value = this.attributes[name]; 
		if(!type) return value;
		switch(type) {
			case "dropdown":
			case "groupeddropdown":
			case "optionset":
				var options = this.getFromSchema(name,"options");
				value = options[value] || value;
				break;
			case "checkbox":
				value = (TkiLib.Utils.castToBool(value)) ? "Y" : "N";
				break;
			default:
				break;
		}
		value = (value === null) ? "n/a" : value;
		return value;
	},
	/* templateData function 
	 * Creates a data object whose properties are available in the view's template.
	 * Passing in an object will extend the object with more properties and/or
	 * override the default included properties.
	 * returns @object
	 */
	templateData: function(extra) {
		var data = {};
		data.id = this.id;
		data.cid = this.cid;
		for(var attr in this.attributes) {
			data[attr] = {};
			data[attr].label = this.getFromSchema(attr,"label") || attr;
			data[attr].value = this.attributes[attr];
			data[attr].formatted = this.getFormattedValue(attr);
		}
		if(!_.isEmpty(extra) && _.isObject(extra)) {
			data = _.extend(data, extra)
		}
		return data;
	},
	
	/* validateProgress function 
	 * Validate all attributes using supplied attribute and moving progress index forward
	 * @ return void
	 */
	validateProgress: function(attr) {
		if(this.debug) this.debug.addToLog("TkiLib.BModel.validateProgress");
		var attr = _.isArray(attr) ? _.last(attr) : attr;
		var attrs = this.attributes;
		var attrKeys = _.keys(attrs);
		var index = _.indexOf(attrKeys,attr);
		if(index > this._progressIndex) this._progressIndex = index;	// Advance the progress index
		var res = true;
		if(this._progressIndex > 0) {
			var opts = {};
			opts.validateAttrs = attrKeys.slice(0,this._progressIndex);
			res = this.hasValidAttributes(attrs,opts);
		}
		return res;
	},
	/* hasValidAttributes function 
	 * Checks the model's attributes - supplied or all
	 * @ return void
	 */
	hasValidAttributes: function(attrKeys) {
		if(this.debug) this.debug.addToLog("TkiLib.BModel.hasValidAttributes");
		if(_.isEmpty(attrKeys)) attrKeys = _.keys(this.attributes);
		attrKeys = _.isArray(attrKeys) ? attrKeys : [attrKeys];
		if(this.debug) this.debug.addToLog("TkiLib.BModel.hasValidAttributes.attrKeys: " + attrKeys.join(','));
		var opts = { validate: true, validateAttrs: attrKeys };
		if(!this._validate(this.attributes, opts)) {
			return false;
		} else {
			return true;
		}
	},
	/* hasRequired function 
	 * Checks if model has required attributes. Does not validate.
	 * @ return void
	 */
	hasRequired: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BModel.hasRequired");
		if(_.isEmpty(this._required)) return true;
		return _.every(this._required,function(val,index) {
			return this.has(val);
		},this);
	},
	/* hasAttributes function 
	 * Checks if model has list of attributes. Can be used to test if a 
	 * model is complete before saving
	 * @ return void
	 */
	hasAttributes: function(attrKeys) {
		attrKeys = _.isEmpty(attrKeys) ? _.keys(this.attributes) : attrKeys;
		if(this.debug) this.debug.addToLog("TkiLib.BModel.hasAttrs");
		return _.every(attrKeys,function(val,index) {
			return this.has(val);
		},this);
	},
	// Check required attributes
	validateRequired: function(attrs,required) {
		if(this.debug) this.debug.addToLog("TkiLib.BModel.validateRequired");
		if(_.isEmpty(required)) return;
		for(var i = 0; i < required.length; ++i) {
			var name = required[i];
			var type = this.getFromSchema(name,"type").toLowerCase();
			var empty = false;
			switch(type) {
				case "checkbox":
				case "numeric":
					if(!attrs[name]) empty = true;
					break;
				default:
					if(TkiLib.Utils.strIsNumeric(attrs[name])) {
						empty = _.isEmpty('' + attrs[name]); // Convert to string for test
					} else {
						empty = _.isEmpty(attrs[name]);
					}
				break;
			}
			if(empty) return ["This field is required",name];
		}
	},
		// Validate attributes using schema
	validateType: function(attrs,toValidate) {
		if(this.debug) this.debug.addToLog("TkiLib.BModel.validateType");
		for(var i = 0; i < toValidate.length; ++i) {
			var name = toValidate[i];
			var schemaData = _.isObject(this.schema[name]) ? this.schema[name] : {};
			var type = this.getFromSchema(name,"type").toLowerCase();
			var value = attrs[name];
			if(!type || value === null) continue;	// Caught by validateRequired
			// Data types
			switch(type) {
				case "numeric":
					if(!TkiLib.Utils.strIsNumeric(attrs[name])) return ["Please enter a valid number",name];
					break;
				case "checkbox":
					var regex = /^[01]$/;
					if(!regex.test(attrs[name])) return ["Invalid value",name];
					break;
				case "text":
					
					break;
				case "dropdown":
				case "optionset":
					var options = this.getFromSchema(name,"options");
					if(_.isObject(options)) {
						// Check if attribute value is a valid option 
						if(!_.has(options,attrs[name])) return ["Please select a valid option",name];
					}
					break;
				case "groupeddropdown":
					var options = this.getFromSchema(name,"options");
					if(_.isObject(options)) {
						// Check if attribute value is a valid option 
						if(!_.has(options,attrs[name])) return ["Please select a valid option",name];
					}
					break;
				default:
					break;
			}
		}
	},
	validate: function(attrs,opts) {
		if(this.debug) this.debug.addToLog("TkiLib.BModel.validate");
		var tmpRequired = _.clone(this._required);
		var attrKeys = _.keys(attrs);
		var toValidate = [];
			// Validate current attribute
		if(opts && opts.validateAttrs && !_.isEmpty(opts.validateAttrs)) {
			toValidate = opts.validateAttrs;
			tmpRequired = _.intersection(tmpRequired,toValidate);
		} else {
			toValidate = attrKeys;
		}
		if(this.debug) this.debug.addToLog("toValidate: " + toValidate.join(','));
			// Allow subclass modification of required
		if(typeof this.modifyRequired == "function") {
			tmpRequired = this.modifyRequired(attrs,toValidate,tmpRequired);
		}
			// Validate required
		var res1 = this.validateRequired(attrs,tmpRequired);
		if(res1) return res1;
			// Validate by schema
		var res2 = this.validateType(attrs,toValidate);
		if(res2) return res2;
			// Custom validation in subclass
		if(typeof this.validateCustom == "function") {   
			var res3 = this.validateCustom(attrs,toValidate);
			if(res3) return res3; 
		}
	}
});
