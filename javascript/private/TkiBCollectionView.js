/* TkiLib.BCollectionView v.0.0.2
 * Copyright (c) 2013 Todd Hossack, Tiraki
 * http://www.tiraki.com/
 * Inspired by Rick Quantz's post
 * http://rickquantz.com/2012/02/rendering-backbone-collections-in-a-view/
 * Dependencies: jQuery, underscore.js, backbone.js
 * Last updated: 2013-05-28
 */
if(typeof(TkiLib) === 'undefined') TkiLib = {};
TkiLib.BCollectionView = Backbone.View.extend({
	/* -------- Instance methods -------- */
	/* initialize function 
	 * 
	 * @ return void
	*/
	initialize: function(options) {
			// Merge in allowed properties from options
		_.extend(this, _.pick(options,this.allowedOptions()));
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.initialize");
			// Setup instance properties
		this._initComplete = false;
		this._activeModel = null;
		this._prevModel = null;
		this._itemViews = {};	// Item view pointers
			// Run init methods
		this.initCollection();
		this.initEvents();
		this.populateView();
		this.render();
	},
	allowedOptions: function() {
		var opts = Backbone.View.prototype.allowedOptions.call(this);
		opts.push('collectionId','collectionItemView','collectionItemViewTpl');
		return opts;
	},
	/* initCollection function 
	 * 
	 * @ return boolean
	*/
   initCollection: function() {
	   if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.initCollection");
			// Collection object passed through options
		if(this.collection) return true;
			// Using collection class
		var cClass = this.options.collectionClass || this.collectionClass || Backbone.Collection;
		var m = this.options.model || this.model || "Backbone.Model";
		this.collection = new cClass(null,{ model: m });
		return true;
   },
   /* initEvents function 
	 * 
	 * @ return boolean
	*/
	initEvents: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.initEvents");
		this.collection.on('add', function(model) {
			this.addToView(model);
			this.render();
			this.initActiveModel();
		},this);
		this.collection.on('remove', function(model) {
			this.removeFromView(model);
		},this);
	},
	/* initActiveModel function 
	 * Caches previous model, instantiates a new model, binds events
	 * @ return void 
	 */
	initActiveModel: function(attrs,opts) {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.initActiveModel");
		attrs = attrs ? _.clone(attrs) : {};
		opts = opts ? _.clone(opts) : { silent: true };
		if(!_.has(opts,'debug')) opts.debug = this.debug;
		this._prevModel = this._activeModel || this.lastItem();	// Assign previous item
		this._activeModel = new this.model(attrs,opts);			// Instantiate new item
		this.trigger("initActiveModel",this._activeModel);
	},
	getActiveModel: function() {
		return this._activeModel;
	},
	 /* populateView function 
	 * 
	 * @ return boolean
	*/
	populateView: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.populateView");
		_.each(this.collection,function(model) {
			this.addToView(model);
		},this);
	},
	/* render function 
	 * 
	 * @ return void
	*/
	render: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.render");
		if(!this.$collectionEl) {
			this.$collectionEl = this.$('#' + this.collectionId);
		}
		this.$collectionEl.html(this.renderCollection());
	},
	/* renderCollection function
	 * 
	 * @ return html
	*/
	renderCollection: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.renderCollection");
		var html = _.map(this._itemViews, function(view) {
			return view.el;
		});
		return html;
	},
	/* addToView function 
	 * 
	 * @ return void
	*/
	addToView: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.addToView");
		var self = this;
		var itemClass = this.collectionItemView || TkiLib.BCollectionItemView;
		var opts = {
			model: model,
			parentView: self,
			templateEl: self.collectionItemViewTpl || null
		};
		if(!_.isEmpty(this.collectionId)) opts.id = this.collectionId + "-" + model.cid;
		var itemView = new itemClass(opts);
		this._itemViews[model.cid] = itemView;
	},
	/* doRemoveAction function 
	 * 
	 * @ return void
	*/
	actionRemove: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.actionRemove");
		evt.preventDefault();
		var cid = evt.target.attributes["data-id"].value;
		if(cid) {
			var model = this.collection.get(cid);
			if(model) this.collection.remove(model);
		}
	},
	/* removeFromView function 
	 * Removes model from view
	 * @ return void
	*/
	removeFromView: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BCollectionView.removeFromView");
		this._itemViews[model.cid].remove();
		delete this._itemViews[model.cid];
	},
	lastItem: function() {
		return (this.collection.length > 0) ? this.collection.at[this.collection.length - 1] : undefined;
	}
});
