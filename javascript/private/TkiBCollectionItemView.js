/* TkiLib.BCollectionItemView v.0.0.2
 * Copyright (c) 2013 Todd Hossack, Tiraki 
 * http://www.tiraki.com/
 * Dependencies: jQuery, underscore.js, backbone.js, TkiLib.Utils
 * Last updated: 2013-05-28
 */
if(typeof(TkiLib) === 'undefined') TkiLib = {};
TkiLib.BCollectionItemView = Backbone.View.extend({
	tagName: "tr",
	initialize: function(options) {
		// Merge in allowed properties from options
		_.extend(this, _.pick(options,this.allowedOptions()));
		if(this.debug) this.debug.addToLog("TkiLib.BEditListItemView.initialize");
		this.templateHtml = (this.templateEl) ? $(this.templateEl).html() : this.defaultTemplate();
		this.template = _.template(this.templateHtml);
		this.render();
	},
	allowedOptions: function() {
		var opts = Backbone.View.prototype.allowedOptions.call(this);
		opts.push('templateEl');
		return opts;
	},
	render: function() {
		//var viewData = (!_.isEmpty(this.parentView.collectionId)) ? { collectionId: this.parentView.collectionId } : null;
		this.$el.html(this.template(this.model.templateData()));
		return this;
    },
	defaultTemplate: function() {
		var data = this.model.templateData();
		if(_.isEmpty(data)) return "";
		var tplKeys = _.keys(data);
		var tpl = "";
		for(var k in data) {
			tpl += "<td><%- "+ k +".formatted %></td>";
		}
		return tpl;
	}
});
