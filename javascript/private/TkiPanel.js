/* TkiLib.Panel v.0.0.3
 * Copyright (c) 2013 Todd Hossack, Tiraki
 * http://www.tiraki.com/
 * Dependencies: jQuery, underscore.js, backbone.js
 * Last updated: 2013-09-10
 */
if(typeof(TkiLib) === 'undefined') TkiLib = {};
TkiLib.Panel = function(options,settings) {
	/* -------- Instance properties -------- */
	_.extend(this, _.pick(options,this.allowedPanelOptions()));
	if(!this.panelHolderId) {
		var holder = this.multiPanelView || this.parentView || null;
		if(holder) {
			this.panelHolderId = _.result(holder,'id');
		} else {
			throw new Error('No panel holder id!');
		}
	}
		// Default settings
	this.panelSettings = {
		minimized: false,
		externalTab: false,
		exclusive: false,
		enabled: true
	}
	if(settings) _.extend(this.panelSettings,settings);
}
_.extend(TkiLib.Panel.prototype,{
	panelEvents: {},
	/* -------- Instance methods -------- */
	initPanel: function() {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.initPanel');
			// Configure options
		this._ensureId();
			// Setup jQuery objects
		this.$panelBody = this.$('.panelBody');
		this.$panelTab = this.findTab();
			// Bind events
		this.$panelTab.on('click','a[class]',_.bind(this.doTabAction,this));
		if(this.panelSettings.externalTab) this.$panelTab.addClass('externalPanelTab');
			// Add to parent view
		if(this.multiPanelView) this.multiPanelView.addPanel(this,this.panelHolderId);
			// Minimize/restore
		var minAction = (this.panelSettings.minimized) ? 'minimize' : 'restore';
		this.handlePanelAction(minAction);
			// Enable/disable
		var enableAction = (this.panelSettings.enabled) ? 'enable' : 'disable';
		this.handlePanelAction(enableAction);
	},
	allowedPanelOptions: function() {
		return ['multiPanelView','panelHolderId'];
	},
	openPanel: function(evt) {
		evt.preventDefault();
	},
	closePanel: function(evt) {
		evt.preventDefault();
	},
	doTabAction: function(evt) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.doTabAction');
		var target, classes, toggleAction;
		//alert('actionShowHidePanel');
		evt.preventDefault();
		if(!this.panelSettings.enabled) return;
		classes = $(evt.currentTarget).attr('class');
		if(!classes) return;
		var res = classes.match(/panelTabAction([\w]+)/i);
		if(res && res[1]) toggleAction = res[1];
		if(!toggleAction) return;
		this.handleToggleAction(toggleAction);
	},
	handleToggleAction: function(toggleAction) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.handleToggleAction: ' + this.id + ' - ' + toggleAction);
		if(!toggleAction) return null;
		var action;
		toggleAction = toggleAction.toLowerCase();
		switch(toggleAction) {
			case 'minrestore':
				action = (this.panelSettings.minimized) ? 'restore' : 'minimize';
				break;
			default:
				break;
		}
		this.handlePanelAction(action);
	},
	handlePanelAction: function(action,args) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.handlePanelAction: ' + action);
		if(!action) return;
		if(this.multiPanelView) this.multiPanelView.handlePanelAction(this,action);	// Pre panel action
		var method = action + 'Panel';
		if(typeof this[method] == 'function') this[method](args);
		if(this.multiPanelView) this.multiPanelView.updatePanelHolder(this,action);	// Post panel action
		return;
	},
	findPanelByTab: function(tab) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.findPanelByTab');
		var $panel, $panelId, $tab;
		$tab = (tab instanceof jQuery) ? tab : $(tab);
		$panelId = $tab.attr('data-panelbody');
		if($panelId.length) $panel = this.$('#'+ $panelId);
		return $panel;
	},
	findTab: function() {
		var $tab = (this.panelSettings.externalTab) ?  $('[data-panel="'+ this.id +'"]') : this.$('.panelTab');
		return $tab;
	},
	restorePanel: function(args) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.restorePanel: ' + this.id);
		if(this.$el.hasClass('panelRestored')) return;	// Already restored
		var removeClasses = 'panelMinimized panelHidden';
		var addClasses = 'panelRestored panelVisible';
		var effect, fxOpts;
			// Supplied Effects
		if(_.isObject(args)) {
			if(_.has(args,'effect')) effect = args.effect;
			if(_.has(args,'options')) fxOpts = args.options;  
		}
			// Fallback to defaults
		if(!effect) { 
			effect = 'toggle';
			fxOpts = true;
		}
		this.$panelBody[effect](fxOpts);
		this.panelSettings.minimized = false;
		this.$el.removeClass(removeClasses).addClass(addClasses);
		this.$panelTab.removeClass(removeClasses).addClass(addClasses);
	},
	minimizePanel: function(args) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.minimizePanel: ' + this.id);
		if(this.$el.hasClass('panelMinimized')) return;	// Already minimized
		var removeClasses = 'panelRestored panelVisible';
		var addClasses = 'panelMinimized panelHidden';
		var effect, fxOpts;
			// Supplied Effects
		if(_.isObject(args)) {
			if(_.has(args,'effect')) effect = args.effect;
			if(_.has(args,'options')) fxOpts = args.options;  
		}
			// Fallback to defaults
		if(!effect) { 
			effect = 'toggle';
			fxOpts = false;
		}
		this.$panelBody[effect](fxOpts);
		this.panelSettings.minimized = true;
		this.$el.removeClass(removeClasses).addClass(addClasses);
		this.$panelTab.removeClass(removeClasses).addClass(addClasses);
	},
	disablePanel: function(args) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.disablePanel: ' + this.id);
		if(this.$el.hasClass('panelDisabled')) return;	// Already disabled
		var removeClasses = 'panelEnabled';
		var addClasses = 'panelDisabled';
		this.panelSettings.enabled = false;
		this.$panelTab.off('click','a',_.bind(this.doTabAction,this));
		this.$el.removeClass(removeClasses).addClass(addClasses);
		this.$panelTab.removeClass(removeClasses).addClass(addClasses);
	},
	enablePanel: function(args) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.enablePanel: ' + this.id);
		if(this.$el.hasClass('panelEnabled')) return;	// Already enabled
		var removeClasses = 'panelDisabled';
		var addClasses = 'panelEnabled';
		this.panelSettings.enabled = true;
		this.$el.removeClass(removeClasses).addClass(addClasses);
		this.$panelTab.removeClass(removeClasses).addClass(addClasses);
	},
	maximizePanel: function(args) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.maximizePanel: ' + this.id);
	}
	/*
	updatePanelTab: function(action) {
		if(this.debug) this.debug.addToLog('TkiLib.Panel.updatePanelTab');
		if(!action) return;
		switch(action) {
			case 'restore':
				this.$panelTab.removeClass('panelMinimized panelHidden')
		.addClass('panelRestored panelVisible');
			case 'minimize':
				this.$panelTab.removeClass('panelRestored panelVisible')
				.addClass('panelMinimized panelHidden');
				break;
			default:
				break;
		}
	}
	*/
});