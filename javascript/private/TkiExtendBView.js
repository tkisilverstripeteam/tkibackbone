/* Extends BackBone.View with some helpful methods
 * Copyright (c) 2013 Todd Hossack, Tiraki
 * http://www.tiraki.com/
 * Dependencies: jQuery, underscore.js, backbone.js
 * Last updated: 2013-05-28
 */

if(Backbone.View) {
	_.extend(Backbone.View.prototype,{
		allowedOptions: function() {
			return ['parentView','debug'];
		},
			// Ensure view has an id
		_ensureId: function() {
			if(!this.id) {
				if(this.$el.attr('id')) {
					this.id = this.$el.attr('id');
				} else {
					this.id = this.cid;
					this.$el.attr('id',this.id);
				}
			}
		}
	});
}