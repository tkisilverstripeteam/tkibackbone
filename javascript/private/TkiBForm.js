/* TkiLib.ModelForm v.0.0.2
 * Copyright (c) 2013 Todd Hossack, Tiraki 
 * http://www.tiraki.com/
 * Dependencies: jQuery, underscore.js, backbone.js, TkiLib.Utils
 * Last updated: 2013-07-18
 */
if(typeof(TkiLib) === 'undefined') TkiLib = {};
TkiLib.BForm = Backbone.View.extend({
		/* -------- Default properties methods -------- */
	formActions: {
		action_submit: null,
		action_createmodel: null,
		action_updatemodel: null,
		action_deletemodel: null
	},
	/* -------- Instance methods -------- */
	/* initialize function 
	 * Sets up object properties from options provided
	 * @return void
	 */
	initialize: function(options) {
			// Merge in allowed properties from options
		_.extend(this, _.pick(options,this.allowedOptions()));
		if(this.debug) this.debug.addToLog("TkiLib.BForm.initialize");
			// Setup instance properties
		this._initComplete = false;
		this._formBusy = false;
		this._actionBusy = false;
		this._ajaxTimeout = 30000;
		this._liveForm = true;
		this._actionInterval = null;
		this._doingModelChanged = false;
		this._SecurityID = null;
		this._gAutoComplete = {};
		this._prevModel = null;
		
		this._messages = {
			user: {},
			app: {}
		};
		this._errors = {
			user: {},
			app: {}
		};
		if(this.collectionView) this.formActions = { action_addmodel: null };
			// Set up form element
		this.$form = (this.formId) ? $('#' + this.formId) : this.$el;
			// Run init methods
		this.initModel();
		if(!this.schema) this.schema = (this.model) ? this.model.schema : {};
		if(!this.formActions) this.formActions = {};
		
		this.initToolTips();
		this.initListeners();
		this.setSecurityID();
		if(this.fetchModelOnInit) {
			this.fetchModel();
		} else {
			if(this.model) {
				this.loadModel(this.model);
			} else {
				this.setupFields();
			}
		}
		this.initFormStatus();
	},
	allowedOptions: function() {
		var opts = Backbone.View.prototype.allowedOptions.call(this);
		opts.push('formName','formId','collectionView','fieldPrefix','relatedFields','fetchModelOnInit','schema','formActions','updateableAttributes');
		return opts;
	},
	url: function() {
		return tkiPageUrl + (this.formName || '');
	},
	/* Events function
	 * Returns default form events. Extend in subclasses to add more events.
	 */
	events: function() {
		var ev = {};
		if(this._liveForm) {
			ev['change select'] = 'handleChange';
			ev['change input'] = 'handleChange';
		}
		if(!_.isEmpty(this.formActions)) {
			_.each(this.formActions,function(v,k) {
				//ev['mousedown #' + this.fieldPrefix + k] = "handleAction";
				ev['click #' + this.fieldPrefix + k] = "handleAction";
				//ev['change #' + this.fieldPrefix + k] = "preventDefaultAction";
				ev['submit'] = 'preventDefaultAction';
			},this);
		}
		return ev;
	},
	/* initListeners function 
	 * Sets general listeners
	 * @ return void
	*/
	initListeners: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.initListeners");
		var self = this;
		if(this.collectionView) {
			this.listenTo(this.collectionView,"initActiveModel",this.loadCollectionModel);
		}
	},
	/* addModelListeners function 
	 * Adds listeners to provided model
	 * @ return void
	*/
	addModelListeners: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.addModelListeners");
		if(!model) return;
		this.listenTo(model,"change",this.modelChanged);
		this.listenTo(model,"error",this.throwUserError);
		this.listenTo(model,"invalid",this.throwUserError);
	},
	/* removeListeners function 
	 * Removes listeners from provided model
	 * @ return void
	*/
	removeModelListeners: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.removeModelListeners");
		this.stopListening(model);
	},
	initToolTips: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.initToolTips");
		var self = this;
		this.$('.tip').each(function() {
			var $btn = $('<span class="tipBtn">?</span>').insertBefore(this);
			// Set tip position relative to button position
			var btnPos = $btn.position();
			var tipX = (btnPos.left) ? btnPos.left + 53 : 53;
			var tipY = (btnPos.top) ? btnPos.top - 25 : -25;
			$(this).css({top: tipY + 'px', left: tipX + 'px'});
				// Set hover event
			$btn.hover(function() {
				$(this).parent().find('.tip').show();
			},function(){
				$(this).parent().find('.tip').hide();
			});
		});
	},
	/* resetForm function 
	 * Resets form, sets default values or values provided
	 * @ return void
	*/
	resetForm: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.resetForm");
		this.clearMessages(_.keys(this.schema));
		this.resetFields();
	},
	/* enableRelatedFields function 
	 * Enables or disables related fields based on the whether the event target 
	 * is true or false, respectively.
	 * @ return void
	*/
	enableRelatedFields: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.enableRelatedFields");
		if(_.isEmpty(evt)) return;
		var targetName = this.getFieldNameById(evt.target.id);
		var targetVal = this.getFieldValue(targetName);
		var fields = (this.relatedFields && this.relatedFields[targetName]) ? this.relatedFields[targetName] : [];
		for(var i=0; i < fields.length; i++) {
			var relatedName = fields[i];
			var relatedVal = this.getFieldValue(relatedName);
			if(TkiLib.Utils.castToBool(targetVal)) { 
				this.enableField(relatedName);
			} else {
				this.disableField(relatedName);
			}
		}
	},
	/* removeReadonlyOnRelatedFields function 
	 * Makes fields readonly or editable based on the whether the event target 
	 * is true or false, respectively.
	 * @ return void
	*/
	makeRelatedFieldsEditable: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.makeRelatedFieldsEditable");
		if(_.isEmpty(evt)) return;
		var targetName = this.getFieldNameById(evt.target.id);
		var targetVal = this.getFieldValue(targetName);
		var fields = (this.relatedFields && this.relatedFields[targetName]) ? this.relatedFields[targetName] : [];
		for(var i=0; i < fields.length; i++) {
			var relatedName = fields[i];
			var relatedVal = this.getFieldValue(relatedName);
			if(TkiLib.Utils.castToBool(targetVal)) {
				this.removeReadonly(relatedName);
			} else {
				this.makeReadonly(relatedName);
			}
		}
	},
	/* populateForm function 
	 * Populates form with values provided in map object using name / value pairs
	 * @ return void
	*/
	populateForm: function(map) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.populateForm");
		if(!map && this.model) map = this.model.attributes;
		if(_.isEmpty(map)) return;	// Empty values
		if(_.isEmpty(this.schema)) return;	// No schema
		if(this.debug) this.debug.addToLog("TkiLib.BForm.populateForm: " + _.keys(map).join(','));
		for(var name in this.schema) {
			if(map[name] !== undefined) this.setFieldValue(name,map[name]);
		}
	},
	/* getFormValues function 
	 * The reverse of populateForm. Gets the form values specified or all if none specified.
	 * @ returns map of attribute keys and values
	*/
	getFormValues: function(attrs) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.getFormValues");
		if(!attrs) attrs = _.keys(this.schema);
		var map = {};
		for(var i=0; i < attrs.length; i++) {
			var attr = attrs[i];
			var attrVal = TkiLib.Utils.trim(this.getFieldValue(attr));	// Attribute value
			if(attrVal == '') attrVal = null;
			map[attr] = attrVal;
		}
		return map;
	},
	/* setSecurityID function 
	 * Stores the SecurityID generated by Silverstripe for use in AJAX requests
	 * @ return void
	*/
	setSecurityID: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setSecurityID");
		this._SecurityID = this.getFieldValue('SecurityID');
	},
	/* getSecurityID function 
	 * Retrieves the stored SecurityID
	 * @ return void
	*/
	getSecurityID: function() {
		return this._SecurityID;
	},
	
	preventDefaultAction: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.preventDefaultAction");
		evt.preventDefault();
	},
	/* handleChange function 
	 * Handles input changes
	 * @ return void
	*/
	handleChange: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.handleChange");
		var id = evt.currentTarget.id;
		var attrKey = this.getFieldNameById(id);	// Attribute
		var attrVal = TkiLib.Utils.trim(this.getFieldValue(attrKey));	// Attribute value
		this.updateAttributes(this.model,attrKey,attrVal);
	},
	/* handleAction function 
	 * Handles form actions
	 * @ return void
	 */
	handleAction: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.handleAction");
		evt.preventDefault();
		var btnAction = evt.currentTarget.name;
		var self = this;
			// Call function after events stack has cleared
		if(_.isEmpty(btnAction) || !_.isString(btnAction)) return;
		var action = btnAction.substr(btnAction.lastIndexOf("_")+1);
		if(_.isEmpty(action) || !_.isString(action)) return;
		
		// Update model with latest form attributes
		var attrs = _.result(this,'updateableAttributes');
		
		if(this.model) {
			this.clearMessages('general');
			var setAttrs = this.getFormValues(attrs);
			this.updateAttributes(this.model,setAttrs,{ validate: false });
			this.model.hasValidAttributes();	// Validate model
		}
		if(this.hasErrors()) return;
		
			// No errors - run action after other actions have finished, including AJAX calls
		var wait = this._ajaxTimeout + 5000;	// 5s more than AJAX timeout
		var period = 100;	// interval

		if(this.isActionBusy()) {
			if(this.debug) this.debug.addToLog("Action busy - waiting");
			this._actionInterval = setInterval(function() {
				self._actionTimer += period;
				if(!self.isActionBusy()) {
					self.delegateAction(action);
					self.clearIntervalById('_actionInterval');
				} else {
					if(self._actionTimer > wait) {
						self.clearIntervalById('_actionInterval');
						self.throwUserError(null,'Action timed out');
					}
				}
			},period);
		} else {
			this.delegateAction(action);
		}
	},
	clearIntervalById: function(id) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.clearIntervalById: " + id);
		if(id && _.has(this,id)) {
			var id = this[id];
			clearInterval(id);
		}
	},
	/* delegateAction function 
	 * Delegates button actions to other methods
	 * @ return void
	 */
	delegateAction: function(action) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.delegateAction");
		if(!action) return;
		var doAjax = true;
		switch(action) {
			case 'createmodel':
				if(this.model && !this.model.isNew()) {
					var mName = _.result(this,'modelName') || 'Item';
					this.throwUserError(null,'Error: ' + mName + ' already created');
					return;
				}
				break;
			case 'addmodel':
				if(!this.hasErrors()) {
					this.collectionView.collection.add(this.model);
				}
				doAjax = false;
				break;
			default:
				case 'fetchmodel':
				case 'updatemodel':
				case 'deletemodel':
				break;
		}
		if(doAjax) return this.doAjaxAction(action);
	},
	/* doAjaxAction function 
	 * Triggered by form action click or programmatically
	 * @ return void
	 */
	doAjaxAction: function(action,opts,silent,actionInUrl) {
		silent = !_.isUndefined(silent) ? silent : false;
		if(this.debug) this.debug.addToLog("TkiLib.BForm.doAjaxAction " + action);
		var self = this;
		if(!opts) opts = {};
		if(!_.has(opts,'data'))	opts.data = this.model.toJSON();
		if(actionInUrl) {
			opts.url = _.result(this,'url') + '/'+ action;
		} else {
			opts.data['action_' + action] = 1;	// Silverstripe forms
		}
			// Trigger event and execute request
		var req = _.bind(this.ajaxRequest,this)
		return _.defer(req,opts,action);
	},
	/* ajaxRequest function 
	 * Performs form action by AJAX, triggers event using name argument
	 * @ return jqXhr object
	*/
	ajaxRequest: function(opts,action) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.ajaxRequest");
		if(_.isEmpty(opts) || !_.has(opts,'data')) return false;
		var self = this;
			// AJAX defaults
		var params = {
			url: _.result(this,'url'),
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data,status,xhr) {
				if(action) self.handleAjaxActionResult('success',action,xhr,status,data);
			},
			error: function(xhr,status,err) {
				if(action) self.handleAjaxActionResult('error',action,xhr,status,err);
			},
			complete: function(xhr,status) {
				if(action) {
					self.setActionNotBusy();
					self.handleAjaxActionResult('complete',action,xhr,status);
				}
			},
			dataType: 'text',
			data: {},
			type: 'POST',
			timeout: this._ajaxTimeout
		}
			// Override by options
		params = _.extend(params,opts);
		params.cache = false;
		params.data.SecurityID = this.getSecurityID();
		
		if(action) {
			this.setActionBusy();
			this.trigger(action,opts);
		}
		
		/* Test latency 
		if(action === 'getemissions') {
			console.log('test latency');
			var xhr = _.delay(function() {
				return Backbone.ajax(params);
			},2000,opts,action);
			return xhr;
		} else {
			return Backbone.ajax(params);
		}
		*/
		return Backbone.ajax(params);
	},
	/* function handleAjaxActionResult
	 * Handles ajax action results generically, but delegates to more 
	 * specific methods if they exist
	 */
	handleAjaxActionResult: function(type,action,xhr,status,res) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.handleAjaxActionResult");
		if(!type || !action) return;
		var parsedRes;
		if(!_.isUndefined(res)) {
			try	{
				parsedRes = Backbone.$.parseJSON(res);	// Json encoded
			} catch(e){
				parsedRes = res;	// Plain text
			}
		}
		
		var resOut = {
			result: null,
			code: null,
			message: null,
			data: null,
			errors: null
		};
		if(_.isObject(parsedRes)) {
				// If the server result is false, switch type to error.
			if(type === 'success' && _.has(parsedRes,'result') && parsedRes.result === 'bad') type = 'error';
			_.extend(resOut,parsedRes);
		} else {
			resOut.message = parsedRes;
			resOut.data = parsedRes;
		}
			// Delegate handling
		var method = action + '_' + type;
		if(_.isFunction(this[method])) {
			this[method].call(this,resOut,xhr,status);
		} else {
			// Trigger event
			this.trigger(action + '_' + type,resOut);
		}
	},
	/* initModel function 
	 * Gets or instantiates the model as provided in the view's options
	 * @ return model 
	 */
	initModel: function(attrs,opts) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.initModel");
		if(this.model) {
				// Already instantiated using options
			if(this.model instanceof Backbone.Model) return;
				// Instantiate model
			var modelClass = this.model;
			this.model = this.makeModel(modelClass,attrs,opts);
			return;
		} else {
				// Model from collectionView
			if(this.collectionView && (_.isFunction(this.collectionView.getActiveModel))) {
				this.model = this.collectionView.getActiveModel();
				return;
			} 
		}
	},
	makeModel: function(modelClass,attrs,opts) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.makeModel");
		if(!_.isFunction(modelClass)) return null;
		attrs = attrs ? _.clone(attrs) : {};
		opts = opts ? _.clone(opts) : { silent: true };
		if(!_.has(opts,'debug')) opts.debug = this.debug;
		return new modelClass(attrs,opts);
	},
	/* loadCollectionModel function 
	 * @ return void
	*/
	loadCollectionModel: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.loadCollectionModel");
		if(!model && this.collectionView && (_.isFunction(this.collectionView.getActiveModel))) {
			var model = this.collectionView.getActiveModel();
		}
		if(model) this.loadModel(model);
	},
	/* loadModel function 
	 * Sets model
	 * @ return void
	*/
	loadModel: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.loadModel");
		if(!model) return;
		if(model !== this.model) {
			this._prevModel = this.model;	// Store previous model
			this.model = model;		// Setup new model
		}

		this.addModelListeners(this.model);
		if(this._prevModel) {
			this.removeModelListeners(this._prevModel);
			this.resetForm();
		}
	
		this.populateForm(this.model.attributes);
		this.setupFields();
		this.trigger("formLoadModel",this.model);
	},
	/* fetchModel function 
	 * @ return void
	 */
	fetchModel: function(id) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.fetchModel");
		var opts = { data: {}};
		this.doAjaxAction('fetchmodel',opts,true,true);
	},
	/* syncModel function 
	 * Like action_updatemodel, but run by application instead and
	 * triggers different events
	 * @ return void
	 */
	syncModel: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.syncModel");
		var attrs = this.model.attributes;
		if(!this.model.hasValidAttributes(attrs)) return;
		var opts = {};
		this.doAjaxAction('syncmodel',opts,true);
	},
	isActionBusy: function() {
		return (this._actionBusy === true);
	},
	setActionBusy: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setActionBusy");
		this._actionBusy = true;
		this._actionTimer = 0;
		this.trigger('actionBusy');
	},
	setActionNotBusy: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setActionNotBusy");
		this._actionBusy = false;
		this.trigger('actionNotBusy');
	},
	initFormStatus: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.initFormStatus");
		var self = this;
		this.$form.prepend('<div class="formStatusBox"><div class="formStatus"><p class="formStatusImage"></p><p class="formStatusMsg"></p></div></div>');
	},
	isFormBusy: function() {
		return (this._formBusy === true);
	},
	/* setFormBusy function
	 * Immediately places container over form to disable user
	 * interaction, then shows status message after specified time,
	 * if form is still busy
	 */
	setFormBusy: function(msg,msgWait) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setFormBusy");
		if(_.isUndefined(msgWait) || _.isNaN(msgWait)) msgWait = 500;
		this._formBusyTimer = 0;
	
		if(!this.isFormBusy()) {
			this._formBusy = true;
			this.trigger('formBusy',msg,msgWait);
			// Wait for a short period first before showing animations, which avoids
			// flashes for low latency responses, and allows registering of the action click event
			var self = this;
			return _.delay(function() {
				if(self.isFormBusy()) {
					self.$form.find('.formStatus').parent('.formStatusBox').show().end()
						.find('.formStatusMsg').text(msg).end().fadeIn(200);
				}
			},msgWait);
		}
		
	},
	setFormNotBusy: function(period) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setFormNotBusy");
		var self = this;
		if(_.isUndefined(period) || _.isNaN(period)) period = 500;
		var $formStatus = this.$form.find('.formStatus');
		var timeout = self._ajaxTimeout + 5000;	// 5s more than AJAX timeout

		var stopFormBusy = _.debounce(function() {
			if(!self.isFormBusy()) {
				$formStatus.fadeOut(300,function() {
					$(this).parent('.formStatusBox').hide();
					$(this).find('.formStatusMsg').empty();
				});
			} else {
				if(self._formBusyTimer > timeout) {
					self.throwUserError(null,'Form timed out');
				}
			}
		},period);
		
		if(this.isFormBusy()) {
			if(this.debug) this.debug.addToLog("Form busy - waiting");
			this._formBusy = false;
			this.trigger('formNotBusy');
			return stopFormBusy();
		}
		
	},
	setFieldBusy: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setFieldBusy");
		var $field = this.getFieldParent(name);
		if($field && !$field.hasClass('fieldBusy')) $field.addClass("fieldBusy");
	},
	setFieldNotBusy: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setFieldNotBusy");
		var $field = this.getFieldParent(name);
		if($field && $field.hasClass('fieldBusy')) $field.removeClass("fieldBusy");
	},
	/* Not needed? If using mousedown on action, event fires before field change */
	pollTextChange: function(evt) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.pollTextChange");
		var self = this;
		var id = evt.currentTarget.id;
		var attr = this.getFieldNameById(id);		// Attribute
		var lastVal = TkiLib.Utils.trim(this.getFieldValue(attr));		// Attribute value
		var attrVal;
		var changed = false;
		var sinceChanged = 0;
		var period = 100;
		this._textInterval = setInterval(function() {
			attrVal = TkiLib.Utils.trim(self.getFieldValue(attr));
			if(attrVal !== lastVal) {	// Changed
				changed = true;
				lastVal = attrVal;
			} else if(changed) {
				sinceChanged += period;
				if(sinceChanged > 2000) {
					self.clearIntervalById('_textInterval');
				}
			}
			self.clearIntervalById('_textInterval');
		},period);
		
		/*
		//if(attrVal == '') attrVal = null;
		var setAttrs = {};	// Attributes to update
		setAttrs[attr] = attrVal;
		var setOpts = {	validate: true,	validateAttrs: [attr]};	// Set options
		if(!_.isEmpty(setAttrs)) this.model.set(setAttrs,setOpts);
		*/

	},
	/* updateAttributes function 
	 * Updates model with form values for supplied fields (attrs), or by default all
	 * fields in schema. Useful for updating and validating model on submit.
	 * @ return void
	*/
	updateAttributes: function(model,attrKey,attrVal,opts) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.updateAttributes");
		var opts, attrKeys, setAttrs, setOpts;
		if(!model || !attrKey) return;
		if (typeof attrKey === 'object') {
			setAttrs = attrKey;
			opts = attrVal;
		} else {
			(setAttrs = {})[attrKey] = attrVal;
		}
		if(this.debug) this.debug.addToLog("updateAttributes: " + _.keys(setAttrs).join(','));
		_.each(setAttrs,function(v,k) {
				// Delegate to specific field update method
			if(_.isFunction(this['updateAttribute' + k])) {
				this['updateAttribute' + k].call(this,model,k,v,opts);
				delete setAttrs[k];
			} else {
				if(setAttrs[k] === '') setAttrs[k] = null;
			}
		},this);
		if(_.isEmpty(setAttrs)) return;
		attrKeys = _.keys(setAttrs);
		setOpts = _.extend({ validateAttrs: attrKeys },(opts || {}));	// Set options
		this.clearMessages(attrKeys);
			// Validate after by default
		var validateBefore = (setOpts.validateBefore || false);
		var validateAfter = true;
		if(_.has(setOpts,'validate') && setOpts.validate === false) {
			validateBefore = false;
			validateAfter = false;
		}
			// Set values
		if(!_.isEmpty(setAttrs)) {
			setOpts.validate = validateBefore;
			model.set(setAttrs,setOpts);
			if(validateAfter) model.hasValidAttributes(attrKeys);	// Validate model if not validated on set
		}
	},
	/* updateableAttributes function 
	 *  Override to set which attribute values should be refreshed from form when clicking action buttons
	 * @ return void
	*/
	updateableAttributes: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.updateAttributes");
		if(this.schema) {
			return _.keys(this.schema);	// Default: all fields in schema
		} else {
			return null;
		}
	},
	/* updateLocationAttrs function 
	 * Updates location of the active model, based on autocomplete value.
	 * @ return void
	 */
	updateLocationAttrs: function(model,attr,place,opts) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.updateLocationAttrs: " + attr);
		if(!model || !attr) return;
		var setAttrs = {}, attrKeys, setOpts = {};	// Attributes and options
		setOpts.validateAttrs = [];
			// Triggered by autocomplete selection
		if(place) {
			// To do here: _gAutoComplete mapView checks
			if(this.mapView) {
				setAttrs[attr] = this.mapView.getPlaceFormattedAddress(place);
				setOpts.validateAttrs.push(attr);
				setOpts.validate = true;
				setAttrs[attr + "LatLng"] = this.mapView.getPlaceLatLng(place);
				setOpts.validateAttrs.push(attr + "LatLng");
					// Get city
				var city = this.mapView.getPlaceCity(place);
				if(city && _.has(city,'short_name')) {
					setAttrs[attr + "City"] = city['short_name'];
				} else {
					setAttrs[attr + "City"] = (city && _.has(city,'long_name')) ? city['long_name'] : null;
				}
				setOpts.validateAttrs.push(attr + "City");
					// Get area
				var area = this.mapView.getPlaceAdminArea(place,1);
				if(area && _.has(area,'short_name')) {
					setAttrs[attr + "Area"] = area['short_name'];
				} else {
					setAttrs[attr + "Area"] = (area && _.has(area,'long_name')) ? area['long_name'] : null;
				}
				setOpts.validateAttrs.push(attr + "Area");
					// Get country
				var country = this.mapView.getPlaceCountry(place);
				if(country && _.has(country,'short_name')) {
					setAttrs[attr + "Country"] = country['short_name'];
				} else {
					setAttrs[attr + "Country"] = (country && _.has(country,'long_name')) ? country['long_name'] : null;
				}
				setOpts.validateAttrs.push(attr + "Country");
			}
		} else {
			// Triggered by deleting input value
			setAttrs[attr] = null;
			setAttrs[attr + "LatLng"] = null;
			setAttrs[attr + "City"] = null;
			setAttrs[attr + "Area"] = null;
			setAttrs[attr + "Country"] = null;
		}
		if(_.isEmpty(setAttrs)) return;
		attrKeys = _.keys(setAttrs);
		setOpts = _.extend({ validateAttrs: attrKeys },(opts || {}));	// Set options
		this.clearMessages(attrKeys);
			// Validate after by default
		var validateBefore = (setOpts.validateBefore || false);
		var validateAfter = true;
		if(_.has(setOpts,'validate') && setOpts.validate === false) {
			validateBefore = false;
			validateAfter = false;
		}
			// Set values
		if(!_.isEmpty(setAttrs)) {
			setOpts.validate = validateBefore;
			model.set(setAttrs,setOpts);
			if(validateAfter) model.hasValidAttributes(attrKeys);	// Validate model if not validated on set
		}
	},
	/* modelChanged function 
	 * Triggered by changes to model. Fires various functions 
	 * based on what has changed based on user input. Any synchronous set calls 
	 * during the execution of this method or its called methods MUST be silent 
	 * to prevent an endless loop.
	 * @ return void
	 */
	modelChanged: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.modelChanged");
		var attrs = model.attributes;
		var changed = model.changedAttributes();
		if(_.isEmpty(changed)) return;
		var changing = this._doingModelChanged;	// Flag to help avoid recursion
		this._doingModelChanged = true;

		/* ---- Methods which update model ---- */
		if(!changing) {}
		if(changing) return;
		
		/* ---- Methods which update form ---- */
		// Update the form after all recursive model changes have completed
		/*
		var populateData = {};
		var toPopulate = [];
		_.each(toPopulate,function(a) {
			if(model.hasChanged(a)) populateData[a] = model.get(a);
		},this);
	
	   if(!_.isEmpty(populateData)) this.populateForm(populateData);
	   */
		this._doingModelChanged = false;
	},
	/* getField function 
	 * Gets field as jQuery element
	 * @ return field jQuery object
	*/
	getField: function(name) {
		if(!name) return null;
		var id = this.getFieldIdByName(name);
		return this.$('#'+id);
	},
	/* getFieldNameById function 
	 * Get field name as substring from id - as generated by Silverstripe
	 * @ return id
	*/
	getFieldNameById: function(id) {
		var name = id.substr(id.lastIndexOf("_")+1);
		return name;
	},
	/* getFieldId function 
	 * Concatenates fieldPrefix with name to get input id as generated by Silverstripe
	 * @ return id
	*/
	getFieldIdByName: function(name) {
		var prefix = this.fieldPrefix;
		return prefix + name;
	},
	/* getFieldValue function 
	 * Gets field value
	 * @ return value
	*/
	getFieldValue: function(name) {
		var type = this.getFieldType(name);
		var $field = this.getField(name);
		var value = null;
		
			// Defaults to text type
		switch(type) {
			case "checkbox":
				value = $field.prop("checked") ? $field.val() : 0;
				break;
			case "text":
			default:
				value = $field.val();
				break;
		}
		return value;
	},
	/* getFieldType function 
	 * Gets field type
	 * @ return type
	*/
	getFieldType: function(name) {
		return this.getFromSchema(name,"type");
	},
	/* setFieldValue function 
	 * Sets field with given value
	 * @ return void
	*/
	setFieldValue: function(name,value) {
		var type = this.getFieldType(name);
		if(this.debug) this.debug.addToLog("TkiLib.BForm.setFieldValue: " + name + ' ' + type + ' ' + value);
		var $field = this.getField(name);
	
			// Defaults to text type
		switch(type) {
			case "checkbox":
				if(value == $field.val()) { 
					$field.prop("checked",true);
				} else {
					$field.prop("checked",false);
				}
				break;
			default:
				$field.val(_.escape(value));
				break;
		}
	},
	/* getFieldLabel function 
	 * Uses field name with name to get input id as generated by Silverstripe
	 * @ return id
	*/
	getFieldLabel: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.getFieldLabel " + name);
		return this.getFromSchema(name,"label");
	},
	/* getFromSchema function 
	 * 
	 * @ return id
	*/
	getFromSchema: function(name,key) {
		var v = "";
		if(_.isObject(this.schema) && _.has(this.schema,name)
			&& _.isObject(this.schema[name]) && _.has(this.schema[name],key)) {
			v = this.schema[name][key];
		}
		return v;
	},
	/* getFieldParent function 
	 * Gets field's parent as jQuery element
	 * @ return field jQuery object
	*/
	getFieldParent: function(name) {
		//if(this.debug) this.debug.addToLog("TkiLib.BForm.getField " + name);
		return this.getField(name).parent();
	},
	/* toggleDisplay function 
	 * Shows/hides element
	 * @ return void
	*/
	toggleDisplay: function(sel,show) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.toggleDisplay " + sel + ' ' + show);
		this.$(sel).toggle(show);
	},
	/* toggleField function 
	 * Shows/hides field
	 * @ return void
	*/
	toggleField: function(name,show) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.toggleField " + name + ' ' + show);
		this.getField(name).toggle(show);
	},
	/* disableFormActions function 
	 * Disables form actions
	 * @ return void
	*/
	disableFormActions: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.disableFormActions");
		if(_.isEmpty(this.formActions)) return;
		_.each(this.formActions,function(v,k) {
			this.disableField(k);
		},this);
	},
	/* enableFormActions function 
	 * Enables form actions
	 * @ return void
	*/
	enableFormActions: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.enableFormActions");
		if(_.isEmpty(this.formActions)) return;
		_.each(this.formActions,function(v,k) {
			this.enableField(k);
		},this);
	},
	/* disableField function 
	 * Disables input
	 * @ return void
	*/
	disableField: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.disableField " + name);
		var $field = this.getField(name);
		if(!$field.prop('disabled')) {
			$field.prop('disabled',true).addClass("disabled");
		}
	},
	/* enableField function 
	 * Enables input
	 * @ return void
	*/
	enableField: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.enableField " + name);
		var $field = this.getField(name);
		if($field.prop('disabled')) {
			$field.prop('disabled',false).removeClass("disabled");
		}
	},
	/* makeReadonly function 
	 * Makes field readonly
	 * @ return void
	*/
	makeReadonly: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.makeReadonly " + name);
		var $field = this.getField(name);
		if(!$field.prop('readonly')) {
			$field.prop('readonly',true).addClass("readonly");
		}
	},
	/* removeReadonly function 
	 * Removes readonly property
	 * @ return void
	*/
	removeReadonly: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.removeReadonly " + name);
		var $field = this.getField(name);
		if($field.prop('readonly')) {
			$field.prop('readonly',false).removeClass("readonly");
		}
	},
	/* showOptional function 
	 * Shows optional note
	 * @ return void
	*/
	showOptional: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.showOptional: " + name);
		if(!name) return;
		var $container = this.getFieldParent(name);
		if(!$container.length) return;
		$container.find('label .optional').show();
	},
	/* hideOptional function 
	 * Hides optional note
	 * @ return void
	*/
	hideOptional: function(name) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.hideOptional: " + name);
		if(!name) return;
		var $container = this.getFieldParent(name);
		if(!$container.length) return;
		$container.find('label .optional').hide();
	},
	/* showMessage function 
	 * Shows form error
	 * @ return void
	*/
	showMessage: function(msg,name,msgType,time) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.showMessage: " + name);
		if(!msg) return;
		if(!msgType) msgType = 'good';
		var $container = null;
			// Get field wrapper
		if(name) $container = this.getFieldParent(name);
			// No field wrapper, use the fieldset element, then try form
		if(!name || !$container.length) {
			$container = this.$("fieldset").first();
			if(!$container.length) $container = this.$form;
		}
			// Get field message holder, if already present
		var $msgHolder = $container.children(".message").first();
		if(!$msgHolder.length) { 
			// No field message holder, so we create one
			$msgHolder = $('<span class="message"></span>').appendTo($container);
		}
		
		$msgHolder.html(msg).addClass(msgType).hide().fadeIn(200);
		if(time) $msgHolder.delay(time).fadeOut(200,function() {
			$(this).empty().removeClass("good bad");
		});
	},
	/* hideMessage function 
	 * Hides messages. Checks for presence of new errors/messages before hiding.
	 * @ return void
	*/
	hideMessages: function(names) {
		if(_.isEmpty(names)) return;
		if(this.debug) this.debug.addToLog("TkiLib.BForm.hideMessages: " + names.join(','));
		var $container;
		for(var i=0; i < names.length; i++) {
			var name = names[i];
				// Don't hide if there are new messages
			/*
			if(type === 'errors' && this.hasErrors(name)) continue;
			if(type === 'messages' && this.hasMessages(name)) continue;
			*/
			if(this.hasErrorsOrMessages(name)) continue;
			if(name == "general") {
				$container = this.$("fieldset").first();
				if(!$container.length) $container = this.$form;
			} else {
				$container = this.getFieldParent(name);
			}
			$container.children(".message").first().fadeOut(200,function() {
				$(this).empty().removeClass("good bad");
			});
		}
	},
	/* clearMessages function 
	 * Clears form error
	 * @ return void
	*/
	clearMessages: function(names,type) {
		var clearedErrors = [];
		var clearedMessages = [];
		if(!names) names = ['general'];
		if(!_.isArray(names)) names = [names];
		if(this.debug) this.debug.addToLog('TkiLib.BForm.clearMessages: ' + names);
		for(var i=0; i < names.length; i++) {
			var name = names[i];
			if((!type || type === 'errors') &&_.has(this._errors.user,name)) {
				delete this._errors.user[name];
				clearedErrors.push(name);
			}
			if((!type || type === 'messages') && _.has(this._messages.user,name)) {
				delete this._messages.user[name];
				clearedMessages.push(name);
			}
		}
		if(!_.isEmpty(clearedErrors)) this.trigger('clearedUserErrors',clearedErrors);
		if(!_.isEmpty(clearedMessages)) this.trigger('clearedUserMessages',clearedMessages);
		var hideMsgs = _.bind(this.hideMessages,this);
		return _.delay(hideMsgs,400,names);	// Remove in form
	},
	/* showUserMessage function 
	 * Shows user message
	 * @ return void
	*/
	showUserMessage: function(msg,name,type,time) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.showUserMessage: " + name);
		if(!msg) return;
		if(!name) name = 'general';
		this._messages.user[name] = msg;
		this.showMessage(msg,name,type,time);
		this.trigger('userMessage',msg,name,type);
	},
	/* clearUserMessage function 
	 * Clears form messages
	 * @ return void
	*/
	clearUserMessages: function(names) {
		if(this.debug) this.debug.addToLog('TkiLib.BForm.clearUserMessages');
		this.clearMessages(names,'messages');
	},
	/* throwUserError function 
	 * Shows form error
	 * @ return void
	*/
	throwUserError: function(model,err) {
		if(this.debug) this.debug.addToLog("TkiLib.BForm.throwUserError");
		var err = _.isArray(err) ? err : [err];
		if(this.debug) this.debug.addToLog(err.join(': '));
		var errMsg = err[0];
		if(!errMsg) return;
		var name = (err[1] && err[1].length) ? err[1] : 'general';
		this._errors.user[name] = errMsg;
		this.showMessage(errMsg,name,'bad');
		this.trigger('userError',model,errMsg,name);
	},
	/* clearUserErrors function 
	 * Clears form error
	 * @ return void
	*/
	clearUserErrors: function(names) {
		if(this.debug) this.debug.addToLog('TkiLib.BForm.clearUserErrors');
		this.clearMessages(names,'errors');
	},
	hasErrors: function(name) {
		if(name) { 
			return _.has(this._errors.user,name);
		} else {
			return (!_.isEmpty(this._errors.user));
		}
	},
	hasMessages: function(name) {
		if(name) {
			return _.has(this._messages.user,name);
		} else {
			return (!_.isEmpty(this._messages.user));
		}
	},
	hasErrorsOrMessages: function(name) {
		return (this.hasErrors(name) || this.hasMessages(name));
	},
	/* setupFields function
	 * Initializes fields on form init or reset.
	 * @ return void
	 */
	setupFields: function() {
		if(this.debug) this.debug.addToLog('TkiLib.BForm.setupFields');
		if(_.isFunction(this.onBeforesetupFields)) this.onBeforesetupFields();
		if(_.isEmpty(this.schema)) return;	// No schema
		for(var name in this.schema) {
			if(this.schema[name].disabled) this.disableField(name);
			if(this.schema[name].readonly) this.makeReadonly(name);
		}
		if(_.isFunction(this.onAftersetupFields)) this.onAftersetupFields();
		this.trigger("setupFieldsDone");
	},
	/* resetFields function
	 * Resets fields within the given element or the whole form if
	 * no element is provided
	 * @ return void
	 */
	resetFields: function(sel) {
		if(this.debug) this.debug.addToLog('TkiLib.BForm.resetFields');
		var $element = (sel) ? this.$(sel) : this.$form;
		$element.find('input[type="radio"],input[type="checkbox"]')
			.prop('checked',false)
		.end()
		.find('option')
			.prop('selected',false)
		.end()
		.find('option:first')
			.prop('selected',true)
		.end()
		.find('input[type="text"],textarea')
			.val('')
		.end();
	}

});

