/* TkiLib.Panels v.0.0.1
 * Copyright (c) 2013 Todd Hossack, Tiraki
 * http://www.tiraki.com/
 * Dependencies: jQuery, underscore.js, backbone.js,TkiExtendBView.js
 * Last updated: 2013-05-28
 */
if(typeof(TkiLib) === 'undefined') TkiLib = {};
TkiLib.BMultiPanelView = Backbone.View.extend({
	/* -------- Instance methods -------- */
	initialize: function(options) {
			// Merge in allowed properties from options
		_.extend(this, _.pick(options,this.allowedOptions()));
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.initialize');
			// Setup instance properties
		this.panels = {};
		this.panelHolders = {};
		this._ensureId();
	},
	getPanelHolderId: function(panel) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.getPanelHolderId');
		return (panel && panel.panelHolderId) ? panel.panelHolderId : null;
	},
	getPanelHolder: function(panel) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.getPanelHolder');
		var hId = this.getPanelHolderId(panel);
		if(!hId) return null;
		if(!_.has(this.panelHolders,hId)) this.initPanelHolder(hId);	// Init holder
		return (_.has(this.panelHolders,hId)) ? this.panelHolders[hId] : null;
	},
	getPanelSiblings: function(panel) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.getPanelSiblings: ' + panel.id);
		if(!panel.panelHolderId
		|| !_.has(this.panels,panel.panelHolderId)) return null;
		return _.omit(this.panels[panel.panelHolderId],panel.id);
	},
	handlePanelAction: function(panel,action) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.handlePanelAction: ' + panel.id + ' - ' + action);
		if(!panel || !action) return;
		var pSiblings = this.getPanelSiblings(panel);
		switch(action) {
			case 'restore':
				if(panel.panelSettings.exclusive) {
					_.each(pSiblings, function(p) {
						p.minimizePanel();
					},this);
				}
				break;
			case 'minimize':
				break;
			default:
				break;
		}
	},
		// Store holder object
	initPanelHolder: function(hId) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.initPanelHolder');
		var $h = this.$('#'+hId);
		if(!$h.length) return;
		this.panelHolders[hId] = $h;
		$h.addClass('panelHolder');
	},
	updatePanelHolder: function(panel,action) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.updatePanelHolder');
		if(!panel) return;
		var $pHolder = this.getPanelHolder(panel);
		if(!$pHolder || !$pHolder.length) return;
		var visible = $pHolder.find('.panelVisible');
		if(!action) return;
		switch(action) {
			case 'restore':
				$pHolder.toggleClass('panelsVisible',true);
				break;
			case 'minimize':
				$pHolder.toggleClass('panelsVisible',(visible.length > 0));
				break;
			default:
				break;
		}
	},
	addPanel: function(panel) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.addPanel');
		if(!panel || !panel.id) return;
		var kv = {};
		kv[panel.id] = panel;
		var hId = this.getPanelHolderId(panel);
		if(!hId) return;	// Throw error?
		this.panels[hId] = _.extend((this.panels[hId] || {}),kv);
		this.updatePanelHolder(panel);
	},
	removePanel: function(panel) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.removePanel');
		if(!panel || !panel.id) return;
		var pId = panel.id;
		var hId = this.getPanelHolderId(panel);
		if(!hId) return;	// Throw error?
		if(_.has(this.panels,hId) && _.has(this.panels[hId],pId)) {
			delete this.panels[hId][pId];
		}
		this.updatePanelHolder(panel);
	},
	iteratePanels: function(setId,action) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.iteratePanels');
		if(!action) return;
		if(setId) {
			if(_.has(this.panels,setId)) {
				_.each(this.panels[setId],function(p) {
					p.handlePanelAction(action);
				},p);
			}
		} else {
			_.each(this.panels,function(v,k) {
				if(!_.isEmpty(v)) {
					_.each(v,function(p) {
						p.handlePanelAction(action);
					},p);
				}
			},this);
		}
	},
	restoreAllPanels: function(setId) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.restoreAllPanels');
		this.iteratePanels(setId,'restore');
	},
	minimizeAllPanels: function(setId) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.restoreAllPanels');
		this.iteratePanels(setId,'minimize');
	},
	isPanel: function(p) {
		if(this.debug) this.debug.addToLog('TkiLib.MultiPanelView.isPanel');
		return (typeof p['initPanel'] == 'function');
	},
	/* 
	 * jLayout interface methods
	 */
	bounds: function(v) {

	},
	preferredSize: function(v) {

	},
	minimumSize: function(v) {

	},
	maximumSize: function(v) {

	},
	isVisible: function(v) {

	},
	doLayout: function(v) {

	}
});
