/* TkiLib.BMapView v.0.0.2
 * Copyright (c) 2013 Todd Hossack todd(at)tiraki.com
 * Dependencies: jQuery, underscore.js, backbone.js
 * Last updated: 2013-05-28
 */

TkiLib.BMapView = Backbone.View.extend({
	/* -------- Instance methods -------- */
	/* initialize function 
	 * Sets up map objects
	 * @return void
	 */
	initialize: function(options) {
			// Merge in allowed properties from options
		_.extend(this, _.pick(options,this.allowedOptions()));
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.initialize");
			// Setup instance properties
		this._initComplete = false;
		this._gmapParams  = { options: {} };
		this._gmapServices = {};
		this._mapIds = {};
		this._mapListeners = {};
		this._activeMarker = null;
		this.prevMarker = null;
		
		var self = this;
		/* gmap3 / Google maps */
		if(!_.isEmpty(this.options.mapOptions)) {
			this._gmapParams.options = _.extend(this._gmapParams.options,options.mapOptions);
		};
			// Set map height based on row height
		//var rowHeight = this.$el.parents('.multiPanelView').height();
		//this.$el.height(rowHeight);
			// Instantiate map
		this.$map = this.$el.gmap3({map : this._gmapParams}); 	// Create jQuery map object
		this.googleMap = this.executeMapCmd({get:{name: "map",full:true}}); // Google map object
	
		this._initComplete = true;
	},
	/* initEvents function 
	 * Custom collection event callbacks
	 * @ return boolean
	*/
	initEvents: function() {
		TkiLib.BEditListView.prototype.initEvents.apply(this);	// Call parent method
			
	},
	/* getGlobalMapParams function 
	 * Gets global map paramaters
	 * @ return object specified by key, or full params if no key specified
	*/
	getGlobalMapParams: function(key) {
		if(key) {
			return (_.has(this._gmapParams,key)) ? this._gmapParams[key] : {};
		} else {
			return this._gmapParams;
		}
	},
	/* getGeocoder function 
	 * Gets Google Maps Geocoder service
	 * @ return object specified by key, or full params if no key specified
	*/
	getGeocoder: function() {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.getGeocoder");
		if(!this._gmapServices.geocoder) {
			this._gmapServices.geocoder = new google.maps.Geocoder();
		}
		return this._gmapServices.geocoder;
	},
	/* geocodeAddress function 
	 * Gets geocoded data from supplied address
	 * @ return 
	*/
	geocodeAddress: function(addr,callback,context) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.geocodeAddress");
		if(!addr || !callback) return null;
		var self = this;
		var args = {};
		if(!context) context = this;
		this.getGeocoder().geocode({address: addr},function(result,status) {
			if(status === google.maps.GeocoderStatus.OK) {
				args['place'] = result[0];
				args['status'] = status;
				callback.call(context,args);
		   }
		});
	},
	/* getPlaceLatLng function 
	 * Gets lat/long
	 * @ return array of lat/lng coordinates
	*/
	getPlaceLatLng: function(place) {
		if(!place || !place.geometry || !place.geometry.location) return null;
		var lat = place.geometry.location.lat()
		var lng = place.geometry.location.lng();
		if(!_.isNumber(lat) || !_.isNumber(lng)) return null;
		return [lat,lng];
		
	},
	/* getPlaceFormattedAddress function 
	 * Gets formatted_address
	 * @ return string of formatted address
	*/
	getPlaceFormattedAddress: function(place) {
		if(!place || !place.formatted_address) return null;
		return place.formatted_address;
	},
	/* getPlaceCity function 
	 * Gets city/town
	 * @return object with city long and short names
	*/
	getPlaceCity: function(place) {
		if(!place || !_.isArray(place.address_components)) return null;
		var addr = place.address_components;
		var city = {};
		_.each(addr,function(v) {
			if(_.has(v,'types') 
			&& (_.indexOf(v.types,'locality') !== -1)) {
				if(_.has(v,'long_name')) city.long_name = v.long_name;
				if(_.has(v,'short_name')) city.short_name = v.short_name;
				return {};
			}
		}, this);
		return !_.isEmpty(city) ? city : null;
	},
	/* getPlaceAdminArea function 
	 * Gets admin area
	 * @return object with area long and short names
	*/
	getPlaceAdminArea: function(place,num) {
		if(!num || (_.indexOf([1,2,3],num) === -1)) num = 1;	// Default
		if(!place || !_.isArray(place.address_components)) return null;
		var addr = place.address_components;
		var area = {};
		_.each(addr,function(v) {
			if(_.has(v,'types') 
			&& (_.indexOf(v.types,'administrative_area_level_' + num) !== -1)) {
				if(_.has(v,'long_name')) area.long_name = v.long_name;
				if(_.has(v,'short_name')) area.short_name = v.short_name;
				return {};
			}
		}, this);
		return !_.isEmpty(area) ? area : null;
	},
	/* getPlaceCountry function 
	 * Gets country
	 * @return object with country long and short names
	*/
	getPlaceCountry: function(place) {
		if(!place || !_.isArray(place.address_components)) return null;
		var addr = place.address_components;
		var country = {};
		_.each(addr,function(v) {
			if(_.has(v,'types') 
			&& (_.indexOf(v.types,'country') !== -1)) {
				if(_.has(v,'long_name')) country.long_name = v.long_name;
				if(_.has(v,'short_name')) country.short_name = v.short_name;
				return {};
			}
		}, this);
		return !_.isEmpty(country) ? country : null;
	},
	/* computeSphericalDistance function 
	 * Computes great circle distance between two lat/lng objects
	 * @ return distance
	*/
	computeSphericalDistance: function(fromLatLngObj, toLatLngObj) {
		if(!fromLatLngObj || !toLatLngObj) return null;
		var d = google.maps.geometry.spherical.computeDistanceBetween(fromLatLngObj, toLatLngObj);
		if(d) d = Math.round((d/1000)*10)/10;	// Convert metres to km
		return d;
	},
	/* addMapIds function
	 * Adds map ids for given model key
	 */
	addMapIds: function(mapKey,ids) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.addMapIds: " + mapKey + ' ' + ids);
		if(_.isString(ids)) ids = [ids];
		this._mapIds[mapKey] = _.union((this._mapIds[mapKey] || []),ids);
	},
	/* addToMapListeners function
	 * Adds map listeners for given model key
	 */
	addToMapListeners: function(mapKey,listeners) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.addToMapListeners");
		if(!mapKey || _.isEmpty(listeners)) return;
		if(!_.has(this._mapListeners,mapKey)) this._mapListeners[mapKey] = {};
		_.each(listeners,function(v,k) {
			this._mapListeners[mapKey][k] = v;
		},this);
	},
	/* removeMapListeners function
	 * Removes specified listeners for given model key. If no listeners are specified,
	 * all listeners for that key are removed. Also removes Google map event listeners.
	 */
	removeMapListeners: function(mapKey,listenerKeys) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.removeMapListeners");
		if(!mapKey || !_.has(this._mapListeners,mapKey)) return;
		if(!listenerKeys) listenerKeys = _.keys(this._mapListeners[mapKey]);
		if(!_.isArray(listenerKeys)) listenerKeys = [listenerKeys];		// String arg
		_.each(listenerKeys,function(v) {
			if(_.has(this._mapListeners[mapKey],v)) {
				var listener = this._mapListeners[mapKey][v];
				if(listener) {
					google.maps.event.removeListener(listener);
				}
				delete this._mapListeners[mapKey][v];
			}
		},this);
	},
	/* getMapIds function
	 * Retrieves map ids using model key
	 */
	getMapIds: function(mapKey) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.getMapIds");
		return (_.has(this._mapIds,mapKey) ? this._mapIds[mapKey] : null);
	},
	/* getMapListeners function
	 * Retrieves map listeners using model key
	 */
	getMapListeners: function(mapKey) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.getMapListeners");
		return (_.has(this._mapListeners,mapKey) ? this._mapListeners[mapKey] : null);
	},
	/* getDrivingDistance function 
	 * Gets the driving distance from direction renderer 
	 * @ return void
	*/
	getDrivingDistance: function(renderer) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.getDrivingDistance");
		var dir = renderer.getDirections();
		var mtrs = 0;	// Distance
		var km = null;
		if(dir.routes && dir.routes[0] && dir.routes[0].legs && dir.routes[0].legs.length) {
			var legs = dir.routes[0].legs;
			for(var i=0; i < legs.length; i++) {
				if(legs[i].distance && legs[i].distance.value) {
					mtrs += legs[i].distance.value;
				}
			}
			km = Math.round((mtrs/1000)*10)/10;
		}
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.getDrivingDistance:km " + km);
		return km;
	},
	/* mapModelKey function 
	 * Creates unique key for keeping track of models on the map
	 * @ return string
	*/
	mapModelKey: function(model) {
		var prefix = (model.mapIdPrefix) ? model.mapIdPrefix + '-' : '';
		return (prefix + model.cid);
	},
	/* mapHasModel function
	 * Retrieves map listeners using model key
	 */
	mapHasModel: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.mapHasModel");
		var mapKey = this.mapModelKey(model);
		return (_.has(this._mapIds,mapKey) || _.has(this._mapListeners,mapKey));
	},
	/* executeMapCmd function
	 * Executes Gmap3 command
	 */
	executeMapCmd: function(cmd) {
		if(_.isEmpty(cmd)) return;
		return this.$map.gmap3(cmd);
	},
	/* addPolyline function 
	 * Adds map marker
	 * @ return void
	*/
	addPolyline: function(mapKey,params) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.addPolyline");
		if(!mapKey) return;
		var pParams = _.extend({
			options: { }
		},(params || {}));
		if(pParams.id) {
			this.executeMapCmd({ polyline: pParams });
			this.addMapIds(mapKey,pParams.id);
		}
	},
	/* removeMapMarker function 
	 * Removes map marker
	 * @ return void
	*/
	removePolyline: function(mapKey,lineId) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.removePolyline");
		if(!mapKey || !lineId) return;
		this.executeMapCmd({clear : { "id": lineId }});
		if(_.has(this._mapIds,mapKey) && _.contains(this._mapIds[mapKey],lineId)) {
			this._mapIds[mapKey] = _.without(this._mapIds[mapKey],lineId);
		}
	},
	/* addMarker function 
	 * Adds map marker
	 * @ return void
	*/
	addMarker: function(mapKey,params) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.addMarker");
		if(!mapKey) return;
		var mParams = _.extend({
			options: { draggable: false, zIndex: 1 }
		},(params || {}));
		if(mParams.id) {
			this.executeMapCmd({ marker: mParams });
			this.addMapIds(mapKey,mParams.id);
		}
	},
	/* addMarkers function 
	 * Adds multiple map markers
	 * @ return void
	*/
	addMarkers: function(mapKey,params) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.addMarkers");
		if(!mapKey) return;
		var mParams = _.extend({
			options: { draggable: false, zIndex: 1 }	// Default options - override per marker
		},(params || {}));
		if(mParams.values) {
			this.executeMapCmd({ marker: mParams });
			var ids = [];
			_.each(mParams.values,function(item) {
				if(_.has(item,'id')) ids.push(item.id);
			},this);
			this.addMapIds(mapKey,ids);
		}
	},
	/* updateMarker function 
	 * Updates map marker
	 * @ return void
	*/
	updateMarker: function(mapKey,params) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.updateMarker");
		if(!mapKey) return;
		var mParams = {	options: { draggable: false, zIndex: 1 }};
		if(params) mParams =  _.extend(mParams,params);
		if(mParams.id) {
			this.executeMapCmd({ marker: mParams });
			this.addMapIds(mapKey,mParams.id);
		}
	},
	/* removeMapMarker function 
	 * Removes map marker
	 * @ return void
	*/
	removeMarker: function(mapKey,markerId) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.removeMarker");
		if(!mapKey || !markerId) return;
		this.executeMapCmd({clear : { "id": markerId }});
		if(_.has(this._mapIds,mapKey) && _.contains(this._mapIds[mapKey],markerId)) {
			this._mapIds[mapKey] = _.without(this._mapIds[mapKey],markerId);
		}
	},
		// Get marker by id
	getMarkerById: function(markerID) {
		var m;
		if(_.isString(markerID) || _.isNumber(markerID)) {
			m = this.executeMapCmd({get: {id: markerID}});
		}
		return m || null;
	},
	setActiveMarker: function(marker) {
		if(this._activeMarker) this.setPrevMarker(this._activeMarker); // Assign previous marker
		if(!(marker instanceof google.maps.Marker)) {
			marker = this.getMarkerById(marker);
		}
		this._activeMarker = marker;
	},
	getActiveMarker: function() {
		return this._activeMarker;
	},
	setPrevMarker: function(marker) {
		if(!(marker instanceof google.maps.Marker)) {
			marker = this.getMarkerById(marker);
		}
		this._prevMarker = marker;
	},
	getPrevMarker: function() {
		return this._prevMarker;
	},
	toggleMarkerBounce: function(marker) {
		if(!(marker instanceof google.maps.Marker)) return false;
		if(marker.getAnimation() != null) {
		  marker.setAnimation(null);
		} else {
		  marker.setAnimation(google.maps.Animation.BOUNCE);
		}
	},
	createMarkerInfoWindow: function(marker,opts) {
		// Create info window
		this.executeMapCmd({
			infowindow:{
				anchor: marker,
				options: opts
			}
		});
		return this.executeMapCmd({get:{name:"infowindow"}});
	},
	getMarkerInfoWindow: function(marker) {
		return this.executeMapCmd({get:{name:"infowindow"}});
	},
	showMarkerInfo: function(marker,event,context,opts) {
		var infoWin = this.getMarkerInfoWindow(marker,event,context);
		if(!infoWin) infoWin = this.createMarkerInfoWindow(marker,opts);
		if(infoWin) {
			infoWin.open(this.googleMap,marker);
			infoWin.setContent(context.data);
		}
	},
	hideMarkerInfo: function(marker) {
		var infoWin = this.getMarkerInfoWindow(marker);
		if(infoWin) infoWin.close();
	},
	/* removeModelFromMap function 
	 * Removes the model and its event listeners from the map
	 * @ return void
	*/
	removeModelFromMap: function(model) {
		if(this.debug) this.debug.addToLog("TkiLib.BMapView.removeModelFromMap");
		var mapKey = this.mapModelKey(model);
			// Remove all event listeners for this map item
		this.removeMapListeners(mapKey);	
		if(!_.has(this._mapIds,mapKey)) return;
			// Remove item from map
		var mapIds = this.getMapIds(mapKey);
		if(!_.isEmpty(mapIds)) {
			for(var i in mapIds) {
				this.executeMapCmd({clear : { "id": mapIds[i]}});
			}
		}
		delete this._mapIds[mapKey];
	}
});
