<?php
// Version 0.1.1
if(!class_exists('TkiSiteVisitor')) {
	user_error(_t('Messages.TKIBACKBONE','The Tiraki Backbone module requires the Tiraki Library (tkilib) module'),E_USER_ERROR);
}

/* To activate the features, add the following to your mysite/_config.php 
 */
//Object::add_extension('','');


if(!function_exists('quote_and_implode')) {
	function quote_and_implode($glue,$arr) {
		$arr = (array) $arr;
		$out = array();
		foreach($arr as $k=>$v) {
			$out[$k] = "'$v'";
		}
		return implode($glue,$out);
	}
}