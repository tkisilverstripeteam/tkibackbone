<?php
/**
  * @author Todd [at] tiraki.com
  * 
  * TkiForm provides methods for integrating Backbone forms on the front end.
  *
  * @package tkilib
  */
class TkiBackboneForm extends TkiForm {
	/* -------- Static properties -------- */
	protected static $form_schemas = array();
	
	public static $model = null;
	
	public static $create_multiple = false;
	
	/*
	public static $allowed_actions = array(
		'fetchmodel'
	);
	
	public static $url_handlers = array(
		'$Action!/$ID' => 'handleAction'
	);
	*/
	public static $success_codes = array(
		'CREATED' => 'Created',
		'FETCHED' => 'Fetched',
		'UPDATEDFOUND' => 'Created'	// To do: change back to already created - updated
	);
	protected static $cached_success_codes;
	
	public static $fail_codes = array(
		'FAIL' => 'Action failed',	// General default
		'NOTCREATED' => 'Not created',
		'ALREADYCREATED' => 'Already created, item updated',
		'NOTFETCHED' => 'Not fetched',
		'NODATA' => 'No data found',
		'INVALIDINPUT' => 'Invalid input',
		'INVALIDOUTPUT' => 'Invalid output'
	);
	protected static $cached_fail_codes;
	
	/* -------- Instance properties -------- */
	protected $action;
	
	protected $requestParams;
	
	protected $errors;
	
	protected $debug = false;
	
	/* -------- Static methods -------- */
	
	/* -------- Instance methods -------- */
	public function setDebug($enable) {
		$this->debug = (bool) $enable;
	}
	public function getDebug() {
		return $this->debug;
	}
	
	public function codeMessage($code,$type) {
		if(empty($code) || empty($type)) return null;
		if($type === 'good') {
			if(empty(self::$cached_success_codes)) {
				self::$cached_success_codes = TkiBackboneExtension::$success_codes;
				$success_codes = $this->stat('success_codes');
				//Debug::dump($success_codes);
				if(!empty($success_codes)) {
					self::$cached_success_codes = array_merge(self::$cached_success_codes,$success_codes);
				}
			}
			if(array_key_exists($code,self::$cached_success_codes)) {
				$message = self::$cached_success_codes[$code];
			} else {
				$message = self::$cached_success_codes['SUCCESS'];
			}
			//Debug::dump(self::$cached_success_codes);
		}
		
		if($type === 'bad') {
			if(empty(self::$cached_fail_codes)) {
				self::$cached_fail_codes = TkiBackboneExtension::$fail_codes;
				$fail_codes = $this->stat('fail_codes');
				//Debug::dump($fail_codes);
				if(!empty($fail_codes)) {
					self::$cached_fail_codes = array_merge(self::$cached_fail_codes,$fail_codes);
				}
			}
			if(array_key_exists($code,self::$cached_fail_codes)) {
				$message = self::$cached_fail_codes[$code];
			} else {
				$message = self::$cached_fail_codes['FAIL'];
			}
			//Debug::dump(self::$cached_fail_codes);
		}
		return _t($this->class .'.'. $code,$message);
	}
	
	public function handleAction($request) {
		$this->action = str_replace("-","_",$request->param('Action'));
		$this->requestParams = $request->requestVars();
		if(!$this->action || !$this->hasAction($this->action)) {
			return $this->httpError(404, "The action '$this->action' does not exist in class $this->class");
		}
		// run & init are manually disabled, because they create infinite loops and other dodgy situations 
		if(!$this->checkAccessAction($this->action) || in_array(strtolower($this->action), array('run', 'init'))) {
			return $this->httpError(403, "Action '$this->action' isn't allowed on class $this->class");
		}
		if($this->hasMethod($this->action)) {
			$result = $this->{$this->action}($request);
			// If the action returns an array, customise with it before rendering the template.
			if(is_array($result)) {
				return $this->getViewer($this->action)->process($this->customise($result));
			} else {
				return $result;
			}
		} else {
			return $this->httpError(403, "Method '$this->action' not found in class $this->class");
		}
	}
	
	/**
	 * Refer to @link Form::httpSubmission
	 * Modified to allow for form submission via JSON
	 */
	function httpSubmission($request) {
		$vars = $request->requestVars();
	
		if(isset($funcName)) {
			Form::set_current_action($funcName);
		}
		
		// Populate the form
		$this->loadDataFrom($vars, true);
		
		// Protection against CSRF attacks
		$token = $this->getSecurityToken();
		if(!$token->checkRequest($request)) {
			$this->httpError(400, "Security token doesn't match, possible CSRF attack.");
		}
		
		// Determine the action button clicked
		$funcName = null;
		foreach($vars as $paramName => $paramVal) {
			if(substr($paramName,0,7) == 'action_') {
				// Break off querystring arguments included in the action
				if(strpos($paramName,'?') !== false) {
					list($paramName, $paramVars) = explode('?', $paramName, 2);
					$newRequestParams = array();
					parse_str($paramVars, $newRequestParams);
					$vars = array_merge((array)$vars, (array)$newRequestParams);
				}
				
				// Cleanup action_, _x and _y from image fields
				$funcName = preg_replace(array('/^action_/','/_x$|_y$/'),'',$paramName);
				break;
			}
		}

		// If the action wasnt' set, choose the default on the form.
		if(!isset($funcName) && $defaultAction = $this->defaultAction()){
			$funcName = $defaultAction->actionName();
		}
			
		if(isset($funcName)) {
			$this->setButtonClicked($funcName);
		}
		
		// Permission checks (first on controller, then falling back to form)
		if(
			// Ensure that the action is actually a button or method on the form,
			// and not just a method on the controller.
			$this->controller->hasMethod($funcName)
			&& !$this->controller->checkAccessAction($funcName)
			// If a button exists, allow it on the controller
			&& !$this->Actions()->fieldByName('action_' . $funcName)
		) {
			return $this->httpError(
				403, 
				sprintf('Action "%s" not allowed on controller (Class: %s)', $funcName, get_class($this->controller))
			);
		} elseif(
			$this->hasMethod($funcName)
			&& !$this->checkAccessAction($funcName)
			// No checks for button existence or $allowed_actions is performed -
			// all form methods are callable (e.g. the legacy "callfieldmethod()")
		) {
			return $this->httpError(
				403, 
				sprintf('Action "%s" not allowed on form (Name: "%s")', $funcName, $this->Name())
			);
		}

		// Validate the form
		if(!$this->validate()) {
			$url = '';
			$code = 'INVALIDINPUT';
			$dataOut = $this->getData();
			
				// Process errors
			if(Director::is_ajax()) {
				
			} else {
				
				if($this->getRedirectToFormOnValidationError()) {
					if($pageURL = $request->getHeader('Referer')) {
						if(Director::is_site_url($pageURL)) {
							// Remove existing pragmas
							$pageURL = preg_replace('/(#.*)/', '', $pageURL);
							$url = $pageURL . '#' . $this->FormName();
						}
					}
				}
			}
			return $this->formResponse($request,false,$dataOut,$code,$url);
		}
		
		// First, try a handler method on the controller (has been checked for allowed_actions above already)
		if($this->controller->hasMethod($funcName)) {
			return $this->controller->$funcName($vars, $this, $request);
		// Otherwise, try a handler method on the form object.
		} elseif($this->hasMethod($funcName)) {
			return $this->$funcName($vars, $this, $request);
		}
		return $this->httpError(404);
	}
	
	/* Refer to @link Session::validate */
	public function validate(){
		if($this->validator) {
			$errors = $this->validator->validate();
			if($errors) return false;
		}
		return true;
	}
	
	public function formSchema() {
		if(!empty(self::$form_schemas[$this->name])) return self::$form_schemas[$this->name];	// Cached
		$schema = array();
		$dataFields = $this->Fields()->dataFields();
		foreach($dataFields as $field) {
			$fName = $field->Name();
			if(!$fName) continue;
			$schema[$fName] = array();
			$schema[$fName]["label"] = $field->Title();	// Label
			$schema[$fName]["type"] = $field->Type();		// Field type
				
			switch($schema[$fName]["type"]) {
				case "dropdown":
				case "optionset":
					//$schema[$key]["dataType"] = "string";	// Data type
					$schema[$fName]["options"] = $field->getSource();		// Options
					break;
				case "groupeddropdown":
					// Flatten options - requires each key/id to be unique 
					// okay if they are all from the same table
					$schema[$fName]["options"] = array();
					$src = $field->getSource();
					foreach($src as $k=>$v) {
						if(is_string($v) || is_numeric($v)) {
							$schema[$fName]["options"][$k] = $v;
						} elseif(is_array($v) && !empty($v['suboptions'])) {
							$schema[$fName]["options"] = $schema[$fName]["options"] + $v['suboptions'];
						}
					}
					
					break;
				case "text":
				case "hidden":
					//$schema[$key]["dataType"] = "string";	// Data type
					break;
				case "checkbox":
				case "numeric":
					$schema[$fName]["dataType"] = "number";	// Data type
					break;
				default:
					break;
			}
			
			if($field->isDisabled()) $schema[$fName]["disabled"] = true;
			if($field->isReadonly()) $schema[$fName]["readonly"] = true;
		}
		self::$form_schemas[$this->name] = $schema;
		return self::$form_schemas[$this->name];
	}
	
	function forTemplate() {
		return $this->renderWith(array(
			$this->getTemplate(),
			'TkiBackboneForm',
			'TkiForm',
			'Form'
		));
	}
	
	protected function getActiveModel($data, $form, $request) {
		// To do: Add id check for editing saved models
		$class = Object::uninherited_static($this->class,'model');
		$result = TkiBackboneExtension::get_visitor_models_by_status($class,'Active',null,'','','',1);
		if($result['data'] && $result['data']->exists()) {
			$result['data'] = $result['data']->First();
		}
		return $result;
	}
	
	/* Override in subclass if specific functionality is needed. Or use
	 * DataObject->onBeforeWrite();
	 */
	protected function saveIntoModel($formData, $form, $model) {
		if(empty($model) || empty($form)) return false;
		$form->saveInto($model);
		$res = $model->write();
		return $res;
	}
	
	protected function formResponse($request,$result,$data=null,$code=null,$url=null,$showMsg=true,$saveData=false) {
		$isAjax = Director::is_ajax();
		$result = (bool) $result;
		$saveData = (bool) $saveData;
		$errors = $this->validator->getErrors();
		$resultType =  ($result) ? 'good' : 'bad';
		if(empty($code)) $code = ($result) ? 'SUCCESS' : 'FAIL';	// Defaults
		$code = strtoupper($code);
			// Prepare message
		if($showMsg) {
			if(is_string($showMsg)) {
				$message = ($showMsg === $resultType) ? $this->codeMessage($code,$resultType) : null;
			} else {
				$message = $this->codeMessage($code,$resultType);
			}
		} else {
			$message = null;
		}
		/* ---- AJAX response ---- */
		if($isAjax) {
				// Prepare output
			$output = array(
				'result' => $resultType,
				'code' => $code,
				'message' => $message,
				'data' => $data
			);
				// Format errors
			if($errors) {
				$errArr = array();
				foreach($errors as $error) {
					$errArr[] = array(
						$error['message'],	// To do: test if escaping necessary. ie Convert::raw2js
						$error['fieldName']
					);
				}
				$output['errors'] = $errArr;
			}
			Session::save();
			$content = json_encode($output,JSON_HEX_TAG);
			$response = new SS_HTTPResponse($content);
				// Determine content type
			$acceptType = ($request) ? $request->getHeader('Accept') : '';
			if(strpos($acceptType, 'application/json') !== FALSE) {
				$response->addHeader('Content-Type', 'application/json');
			} else {
				$response->addHeader('Content-Type', 'text/plain');
			}
			return $response;
		} else {
		/* ---- Normal response ---- */
				// Load data and errors into session and redirect
			$sesData = array();
			if($errors || empty($url) || $saveData) {
				$sesData['data'] = $data;
			} else {
				$sesData['data'] = null;
			}
			
			if($errors) $sesData['errors'] = $errors;
			Session::set("FormInfo.{$this->FormName()}",$sesData);
				// Message
			if($showMsg === true || (is_string($showMsg) && $showMsg === $resultType)) {
				$this->sessionMessage($message, $resultType);
			}
			Session::save();
			
			if($this->debug) exit;
			if(!empty($url)) {
				Director::redirect($url);
			} else {
				Director::redirectBack();
			}
			return $result;
		}
	}
	
	/* -------- Actions --------- */
	/* Create model in database
	 */
	public function createmodel($data, $form, $request) {
			// Initialise vars
		$result = null;
		$dataOut = null;
		$code = null;
			// Create model
		$dataIn = $form->getData();
		$modelResult = $this->getActiveModel($dataIn, $form, $request);
			// Disallow multiple model creation attempts, unless allowed 
			// through create_multiple static
		if(!$this->stat('create_multiple') && !empty($modelResult['data'])) {
			$result = true;
			$code = 'UPDATEDFOUND';
			$dataOut = $this->saveIntoModel($dataIn, $form, $modelResult['data']);
		} else {
			$modelClass = $this->uninherited('model');
			$model = !empty($modelClass) ? Object::create($modelClass) : null;
			$dataOut = $this->saveIntoModel($dataIn, $form, $model);
			if($dataOut && is_int($dataOut)) {
				$result = true;
				$code = 'CREATED';
			} else {
				$result = false;
				$code = 'NOTCREATED';
			}
		}
		return $this->formResponse($request,$result,$dataOut,$code);
	}
	
	/* Fetches model from database
	 */
	public function fetchmodel($request) {
			// Initialise vars
		$result = null;
		$dataOut = null;
		$code = null;
			// Find model
		$modelResult = $this->getActiveModel(null, null, $request);
		
		if($modelResult['data'] && $modelResult['data']->hasMethod('toBackboneMap')) {
			$result = true;
			$code = 'FETCHED';
			$dataOut = $modelResult['data']->toBackboneMap();	
		} else {
			$result = false;
			$code = 'NOTFETCHED';
		}
		
		return $this->formResponse($request,$result,$dataOut,$code);
	}
	/* Updates model in database
	 */
	public function updatemodel($data, $form, $request) {
			// Initialise vars
		$result = null;
		$dataOut = null;
		$code = null;
			// Update model
		$formData = $form->getData();

		$modelResult = $this->getActiveModel($formData, $form, $request);
		if(!$modelResult['data']) {
			$result = false;
			$code = 'NOTFOUND';
		} else {
			$dataOut = $this->saveIntoModel($formData, $form, $modelResult['data']);
			if($dataOut && is_int($dataOut)) {
				$result = true;
				$code = 'UPDATED';
			} else {
				$result = false;
				$code = 'NOTUPDATED';
			}
		}
		return $this->formResponse($request,$result,$dataOut,$code);
	}

	/* Wrapper for updatemodel. Triggers different events in the frontend.
	 */
	public function syncmodel($data, $form, $request) {
		return $this->updatemodel($data,$form,$request);
	}

}