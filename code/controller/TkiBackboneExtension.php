<?php
/**
  * @author Todd [at] tiraki.com
  * 
  * TkiBackboneExtension provides controller methods for integrating Backbone
  *
  * @package tkilib
  */
class TkiBackboneExtension extends Extension {
	/* -------- Static properties -------- */
	
	/*
	public static $allowed_actions = array(
		'fetchmodel'
	);
	
	public static $url_handlers = array(
		'$Action!/$ID' => 'handleAction'
	);
	*/
	
	public static $success_codes = array(
		'SUCCESS' => 'Action successful',	// General default
		'FOUND' => 'Found',
		'CREATED' => 'Created',
		'SOMECREATED' => 'Some records created',
		'ALLREADABLE' => 'All records readable',
		'SOMEREADABLE' => 'Some records readable',
		'UPDATED' => 'Updated',
		'SOMEUPDATED' => 'Some records updated',
		'DELETED' => 'Deleted',
		'SOMEDELETED' => 'Some deleted'
	);
	protected static $cached_success_codes;
	
	public static $fail_codes = array(
		'FAIL' => 'Action failed',	// General default
		'ERROR' => 'Action error',
		'NOCLASS' => 'No class given',
		'NOFILTER' => 'No filter given',
		'NOID' => 'No id(s) given',
		'NOTLOGGEDIN' => 'You are not logged in',
		'NOUSER' => 'No user found',
		'NOSTATUS' => 'No status given',
		'NOTFOUND' => 'Not found',
		'NOMODELSGIVEN' => 'No model(s) given',
		'NOTCREATED' => 'Not created',
		'NOTREADABLE' => 'Not readable',
		'NOTUPDATED' => 'Not updated',
		'NOTDELETED' => 'Not deleted'
	);
	protected static $cached_fail_codes;
	
	/* -------- Instance properties -------- */
	protected $actionMessage;
	protected $actionMessageType;
	
	/* -------- Static methods -------- */
	/* ---- Messages ---- */
	public static function code_message($code,$type) {
		if(empty($code) || empty($type)) return null;
		if($type === 'good') {
			if(array_key_exists($code,self::$success_codes)) {
				$message = self::$success_codes[$code];
			} else {
				$message = self::$success_codes['SUCCESS'];
			}
			return _t('TkiBackboneExtension.'. $code,$message);
		}
		if($type === 'bad') {
			if(array_key_exists($code,self::$fail_codes)) {
				$message = self::$fail_codes[$code];
			} else {
				$message = self::$fail_codes['FAIL'];
			}
		}
		return _t('TkiBackboneExtension.'. $code,$message);
	}
	
	/* ---- Base methods ---- */
	
	/* Low level method to get models from the database. 
	 * Uses VisitorID/MemberID to find models
	 * Note: Does not check permissions.
	 * returns array with boolean result, code and DataObjectSet;
	 */
	
	public static function get_models($class,$filter,$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class) || empty($filter)) {
			$output['code'] = empty($class) ? 'NOCLASS' : 'NOFILTER';
			return $output;
		}
			// Base query items
		$class = Convert::raw2sql($class);
		$models = DataObject::get($class,$filter,$sort,$join,$limit);
		if($models && $models->exists()) {
			$output['code'] = 'FOUND';
			$output['result'] = true;
			$output['data'] = $models;
		} else {
			$output['code'] = 'NOTFOUND';
		}
		return $output;
	}
	
	protected static function get_visitor_filter($id=null) {
		if(!$id) $id = TkiSiteVisitor::current_visitor_id();
		return ($id) ? "\"VisitorID\"={$id}" : '';
	}
	
	protected static function get_member_filter($id=null) {
		if(!$id) $id = Member::currentUserID();
		return ($id) ? "\"MemberID\"={$id}" : '';
	}
	
	protected static function get_ids_filter($class,$ids) {
		$ids = (array) $ids;
		$prepIds = array();
		foreach($ids as $id) {
			$prepIds[] = (int) $id;	// Cast as integer
		}
		$idSql = implode(',',$prepIds);
		return (!empty($idSql)) ? "\"$class\".\"ID\" IN ($idSql)" : '';
		
	}
	
	protected static function get_status_filter($status) {
		$status = Convert::raw2sql($status);
		$status = (array) $status;
		$statusSql = quote_and_implode(',',$status);
		return (!empty($statusSql)) ? "\"Status\" IN ({$statusSql})" : '';
	}
	
	/* ---- Visitor select methods ---- */
	public static function get_visitor_models_by_id($class,$ids,$visitorId=null,$extraFilter='',$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class) || empty($ids)) {
			$output['code'] = empty($class) ? 'NOCLASS' : 'NOID';
			return $output;
		}
		$filter = array(self::get_ids_filter($class,$ids));
		$visitorFilter = self::get_visitor_filter($visitorId);
		if(empty($visitorFilter)) {
			$output['code'] = 'NOUSER';
			return $output;
		} else {
			$filter[] = $visitorFilter;
		}
		if(!empty($extraFilter)) $filter[] = $extraFilter;
		return self::get_models($class,$filter,$sort,$join,$limit);
	}
	
	public static function get_visitor_models_by_status($class,$status,$visitorId=null,$extraFilter='',$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class) || empty($status)) {
			$output['code'] = empty($class) ? 'NOCLASS' : 'NOSTATUS';
			return $output;
		}
		// To do: filter out expired session calculations
		$filter =
		$filter = array(self::get_status_filter($status));
		$visitorFilter = self::get_visitor_filter($visitorId);
		if(empty($visitorFilter)) {
			$output['code'] = 'NOUSER';
			return $output;
		} else {
			$filter[] = $visitorFilter;
		}
		if(!empty($extraFilter)) $filter[] = $extraFilter;
		return self::get_models($class,$filter,$sort,$join,$limit);
	}
	
	public static function get_all_visitor_models($class,$visitorId=null,$extraFilter='',$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class)) {
			$output['code'] = 'NOCLASS';
			return $output;
		}
		$filter = array(self::get_visitor_filter($visitorId));
		if(empty($filter)) {
			$output['code'] = 'NOUSER';
			return $output;
		}
		if(!empty($extraFilter)) $filter[] = $extraFilter;
		return self::get_models($class,$filter,$sort,$join,$limit);
	}
	
	 /* ---- Member select methods ---- */
	public static function get_member_models_by_id($class,$ids,$memberId=null,$filter='',$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class) || empty($ids)) {
			$output['code'] = empty($class) ? 'NOCLASS' : 'NOID';
			return $output;
		}
		$filter = array(self::get_ids_filter($class,$ids));
		$memberFilter = self::get_member_filter($memberId);
		if(empty($memberFilter)) {
			$output['code'] = 'NOUSER';
			return $output;
		} else {
			$filter[] = $memberFilter;
		}
		return self::get_models($class,$filter,$sort,$join,$limit);
	}
	
	public static function get_member_models_by_status($class,$status,$memberId=null,$filter='',$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class) || empty($status)) {
			$output['code'] = empty($class) ? 'NOCLASS' : 'NOSTATUS';
			return $output;
		}
		$filter = array(self::get_status_filter($status));
		$memberFilter = self::get_member_filter($memberId);
		if(empty($memberFilter)) {
			$output['code'] = 'NOUSER';
			return $output;
		} else {
			$filter[] = $memberFilter;
		}
		return self::get_models($class,$filter,$sort,$join,$limit);
	}
	
	public static function get_all_member_models($class,$memberId=null,$filter='',$sort='',$join='',$limit='') {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($class)) {
			$output['code'] = 'NOCLASS';
			return $output;
		}
		
		$memberFilter = array(self::get_member_filter($memberId));
		if(empty($memberFilter)) {
			$output['code'] = 'NOUSER';
			return $output;
		}
		
		return self::get_models($class,$memberFilter,$sort,$join,$limit);
	}
	/* -------- Methods with permission checks -------- */
	/* ---- Create methods ---- */
	public static function create_models($models,$data=array()) {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(empty($models)) {
			$output['code'] = 'NOMODELSGIVEN';
			return $output;
		}
		if(!($models instanceof DataObjectSet)) {
			$set = new DataObjectSet();
			$set->push($models);
			$models = $set;
		}
		$created = array();
		$modelCount = $models->Count();
		foreach($models as $model) {
			if($model->canCreate()) {
				$model->write();
				if(!empty($data) && is_array($data)) $model->update($data);
				$res = $model->write();
				if($res) array_push($created,$res);
			}
		}
		$output['data'] = implode(',',$created);
		$numCreated = count($created);
		if($numCreated > 0 && $numCreated < $modelCount) {
			$output['code'] = 'SOMECREATED';
			$output['result'] = true;
		} elseif($numCreated === 0) {
			$output['code'] = 'NOTCREATED';
		} else {
			$output['code'] = 'CREATED';
			$output['result'] = true;
		}
		return $output;
	}
	
	/* ---- Read methods ---- */
	public static function read_models($models) {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(!$models || !$models->exists()) {
			$output['code'] = 'NOTFOUND';
			return $output;
		}
		if($models && $models->exists()) {
			if(!($models instanceof DataObjectSet)) {
				$set = new DataObjectSet();
				$set->push($models);
				$models = $set;
			}
			$outCount = 0;
			$modelCount = $models->Count();
			$output['data'] = new DataObjectSet();
			foreach($models as $model) {
				if($model->canView()) { 
					$output['data']->push($model);
					++$outCount;
				}
			}
			if($outCount > 0 && $outCount < $modelCount) {
				$output['code'] = 'SOMEREADABLE';
				$output['result'] = true;
			} elseif($outCount === 0) {
				$output['code'] = 'NOTREADABLE';
			} else {
				$output['code'] = 'ALLREADABLE';
				$output['result'] = true;
			}
		} else {
			$output['code'] = 'NOTFOUND';
		}
		//if($code) error_log(__METHOD__.':'.$class .' '. $code .' '.$_SERVER['REMOTE_ADDR']);
		return $output;
	}
	
	/* ---- Update methods ---- */
		// Returns array of results, including IDs of updated records
	public static function update_models($models,$data=array()) {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		if(!$models || !$models->exists()) {
			$output['code'] = 'NOTFOUND';
			return $output;
		}
		if(!($models instanceof DataObjectSet)) {
			$set = new DataObjectSet();
			$set->push($models);
			$models = $set;
		}
		$updated = array();
		$modelCount = $models->Count();
		foreach($models as $model) {
			if($model->canEdit()) {
				if(!empty($data) && is_array($data)) $model->update($data);
				$res = $model->write();
				if($res) array_push($updated,$res);
			}
		}
		$output['data'] = implode(',',$updated);
		$numUpdated = count($updated);
		if($numUpdated > 0 && $numUpdated < $modelCount) {
			$output['code'] = 'SOMEUPDATED';
			$output['result'] = true;
		} elseif($numUpdated === 0) {
			$output['code'] = 'NOTUPDATED';
		} else {
			$output['code'] = 'UPDATED';
			$output['result'] = true;
		}
		return $output;
	}
	
	/* ---- Delete methods ---- */
	// Returns array of results, including IDs of deleted records
	public static function delete_models($models) {
		$output = array(
			'result' => false,
			'code' => null,
			'data' => null
		);
		//Debug::dump($models);
		if(!$models || !$models->exists()) {
			$output['code'] = 'NOTFOUND';
			return $output;
		}
		if(!($models instanceof DataObjectSet)) {
			$set = new DataObjectSet();
			$set->push($models);
			$models = $set;
		}
		$deleted = array();
		$modelCount = $models->Count();
		foreach($models as $model) {
			if($model->canDelete()) {
				array_push($deleted,$model->ID);
				$model->delete();
			}
		}
		$output['data'] = implode(',',$deleted);
		$numDeleted = count($deleted);
		if($numDeleted > 0 && $numDeleted < $modelCount) {
			$output['code'] = 'SOMEDELETED';
			$output['result'] = true;
		} elseif($numDeleted === 0) {
			$output['code'] = 'NOTDELETED';
		} else {
			$output['code'] = 'DELETED';
			$output['result'] = true;
		}
		if(!empty($deleted)) $output['data'] = implode(',',$deleted);
		return $output;
	}
	
	/* -------- Instance methods -------- */	
	
	/* ---- Actions ---- */
	protected function finishAction($request,$result,$data=null,$code=null,$url=null,$silent=false) {
		$isAjax = Director::is_ajax();
		$result = (bool) $result;
		$resultType =  ($result) ? 'good' : 'bad';
		if(empty($code)) $code = ($result) ? 'SUCCESS' : 'FAIL';	// Defaults
		$code = strtoupper($code);
			// Prepare message
		if(!$silent) {
			$message = self::code_message($code,$resultType);
		} else {
			$message = null;
		}
		/* ---- AJAX response ---- */
		if($isAjax) {
				// Prepare output
			$output = array(
				'result' => $resultType,
				'code' => $code,
				'message' => $message,
				'data' => $data
			);
			Session::save();
			$content = json_encode($output,JSON_HEX_TAG);
			$response = new SS_HTTPResponse($content);
				// Determine content type
			$acceptType = ($request) ? $request->getHeader('Accept') : '';
			if(strpos($acceptType, 'application/json') !== FALSE) {
				$response->addHeader('Content-Type', 'application/json');
			} else {
				$response->addHeader('Content-Type', 'text/plain');
			}
			return $response;
		} else {
		/* ---- Normal response ---- */
				// Message
			if(!$silent) $this->sessionActionMessage($message, $resultType);
			Session::save();
			// Load errors into session and post back
			
			if(!empty($url)) {
				Director::redirect($url);
			} else {
				Director::redirectBack();
			}
			return $result;
		}
	}
	
	/* ---- Action message methods ---- 
	 * Borrowed and adapted from Form.php 
	 */

	function ActionMessage() {
		//Debug::dump('ActionMessage');
		$this->getActionMessageFromSession();
		//Debug::dump($this->actionMessage);
		$message = $this->actionMessage;
		$this->clearActionMessage();
		return (string) $message;
	}
	
	/**
	 * @return string
	 */
	public function ActionMessageType() {
		//Debug::dump('ActionMessageType');
		//Debug::dump($this->action);
		$this->getActionMessageFromSession();
		//Debug::dump($this->actionMessageType);
		$type = $this->actionMessageType;
		$this->clearActionMessageType();
		return (string) $type;
	}
	
	protected function sessionActionMessage($message, $type) {
		Session::set("{$this->owner->class}.actionMessage", $message);
		Session::set("{$this->owner->class}.actionMessageType", $type);
	}
	
	protected function clearActionMessage() {
		//Debug::dump('clearActionMessage');
		$this->actionMessage  = null;
		Session::clear("{$this->owner->class}.actionMessage");
	}
	
	protected function clearActionMessageType() {
		//Debug::dump('clearActionMessageType');
		$this->actionMessageType  = null;
		if($this->owner->action !== 'index') Session::clear("{$this->owner->class}.actionMessageType");
	}
	
	protected function getActionMessageFromSession() {
		//Debug::dump('getActionMessageFromSession');
		if($this->actionMessage || $this->actionMessageType) {
			return $this->actionMessage;
		} else {
			$this->actionMessage = Session::get("{$this->owner->class}.actionMessage");
			$this->actionMessageType = Session::get("{$this->owner->class}.actionMessageType");
		}
	}
}